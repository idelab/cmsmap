<?php
// $Id$

/**
 * @file
 * Defines a style plugin that will display points on a map from a number of
 * different providers.
 */
class mapstraction_views_style_map extends views_plugin_style {
  function option_definition() {
    
    $options = parent::option_definition();
    
    $options['dimensions'] = array(
      'default' => '600x400'
    );
    $options['initial_point'] = array(
      'default' => array(
        'auto' => TRUE,
        'latitude' => '',
        'longitude' => '',
        'zoom' => 10,
      	'auto_zoom' => 6,
    ));
    
    $options['geocode'] = array(
      'default' =>array(
        'provider' =>'none',
        'zoom' => 10,
    ));
    
    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    $form['api'] = array(
      '#type' => 'select',
      '#title' => t('Mapping API'),
      '#description' => t('Select the Mapstraction API to use for this view.'),
      '#options' => mapstraction_cck_available_providers(),
      '#default_value' => $this->options['api'],
    );
    
    $form['dimensions'] = array(
      '#type' => 'fieldset',
      '#title' => t('Map Dimensions'),
      '#description' => t('Set the map dimensions. It must be a valid CSS value, for example 400px or 80%'),
    );
    $form['dimensions']['width'] = array(
    '#title' => t('Width'),
    '#type' => 'textfield',
    '#default_value' => isset($this->options['dimensions']['width'])?$this->options['dimensions']['width']: '600px',
    '#size' => 5,
    '#prefix' => "<div class='container-inline'>",
    
    );
    $form['dimensions']['height'] = array(
      '#title' => t('Height'),
      '#type' => 'textfield',
      '#default_value' => isset($this->options['dimensions']['height'])?$this->options['dimensions']['height']: '400px',
      '#size' => 5,
      '#suffix' => '</div>',
    );
      
    $form['initial_point'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => t('Initial point'),
    );

    $form['initial_point']['auto'] = array(
      '#type' => 'checkbox',
      '#title' => t('Auto detect initial point'),
      '#description' => t('If checked, the map will auto detect initial coordinates and zoom level based on all markers on the map. Otherwise you can specify these values by yourself.'),
      '#default_value' => isset($this->options['initial_point']['auto'])?$this->options['initial_point']['auto']: TRUE,
    );

    $form['initial_point']['latitude'] = array(
      '#type' => 'textfield',
      '#title' => t('Latitude'),
      '#size' => 40,
      '#maxlength' => 255,
      '#process' => array('views_process_dependency'),
      '#dependency' => array(
        'edit-style-options-initial-point-auto' => array(0)
      ),
      '#default_value' => $this->options['initial_point']['latitude'],
    );

    $form['initial_point']['longitude'] = array(
      '#type' => 'textfield',
      '#title' => t('Longitude'),
      '#size' => 40,
      '#maxlength' => 255,
      '#process' => array('views_process_dependency'),
      '#dependency' => array(
        'edit-style-options-initial-point-auto' => array(0)
      ),
      '#default_value' => $this->options['initial_point']['longitude'],
    );

    $form['initial_point']['zoom'] = array(
      '#type' => 'textfield',
      '#title' => t('Zoom level'),
      '#size' => 2,
      '#maxlength' => 2,
      '#process' => array('views_process_dependency'),
      '#dependency' => array(
        'edit-style-options-initial-point-auto' => array(0)
      ),
      '#default_value' => $this->options['initial_point']['zoom'],
    );
    
    $form['initial_point']['auto_zoom'] = array(
      '#type' => 'textfield',
      '#title' => t('Default Zoom level'),
      '#size' => 2,
      '#maxlength' => 2,
      '#description' => t('Select the default zoom level for the map when there are only one feature to display.'),
      '#process' => array('views_process_dependency'),
      '#dependency' => array(
        'edit-style-options-initial-point-auto' => array(1)
      ),
      '#default_value' => $this->options['initial_point']['auto_zoom'],
    );
    
    $form['zoom_control'] = array(
      '#type' => 'select',
      '#title' => t('Zoom Control'),
      '#options' => array(
        'large' => t('Large'),
        'small' => t('Small'),
        'none' => t('None')
      ),
      '#default_value' => $this->options['zoom_control'],
    );
    if(module_exists('mapstraction_cck_geocoder'))
      $form['geocode'] = mapstraction_cck_geocoder_settings($this->options);
    $handlers = $this->display->handler->get_handlers('field');
    if (empty($handlers)) {
      $form['error_markup'] = array(
        '#value' => t('You need at least one field before you can configure your field settings'),
        '#prefix' => '<div class="error form-item description">',
        '#suffix' => '</div>',
      );
    }
    else {
      $field_names[$field] = array('' => '--');
      foreach ($handlers as $field => $handler) {
        if ($label = $handler->label()) {
          $field_names[$field] = $label;
          if(isset($handler->geo))$geo_fields[$field] = $label;
        }
        else {
          $field_names[$field] = $handler->ui_name();
          if(isset($handler->geo))$geo_fields[$field] = $handler->ui_name();
        }
        
      }

      $form['fields'] = array(
        '#type' => 'fieldset',
        '#title' => 'Field usage',
        '#description' => t('Select the fields that will be showed in the map. In the Bubble field you can choose the field that will be displayed when someone clicks over a marker. Then, select the geospatial fields that will be displayed on the map.'),
      );

      $form['fields']['wkt'] = array(
      	'#type' => 'checkboxes',
        '#title' => t('Geographic fields'),
        '#description' => t('Choose the fields that will be showed in the map. <b>You must select WKT fields</b>'),
        '#options' => $geo_fields,
        '#default_value' => isset($this->options['fields']['wkt'])?$this->options['fields']['wkt']:array(),
        '#required' => TRUE,
      );
    }
  }
    
  function options_validate($form,&$form_state) {
    
    $pattern = "/^[0-9]+(px|%)$/";
    $values = array('height' => t('Height'), 'width' => t('Width'));
    foreach ($values as $key => $value) {
      if(preg_match($pattern,$form_state['input']['style_options']['dimensions'][$key])){
        if($widget[$key] != ereg_replace("%","",$widget[$key])){
          if((int)ereg_replace("%","",$widget[$key]) > 100)
          form_set_error('style_options][dimensions]['.$key, t('The @field value isn\'t correct. It mustn\'t bigger than 100%',array('@field'=>$value)));
        }
      }else{
        form_set_error('style_options][dimensions]['.$key, t('The @field value isn\'t correct. It must be a correct CSS value',array('@field'=>$value)));
      }
    }
    
    if($form_state['input']['style_options']['initial_point']['auto']){
      if(!is_numeric($form_state['input']['style_options']['initial_point']['auto_zoom']) ||
          ($form_state['input']['style_options']['initial_point']['auto_zoom'] <=0 ||
          $form_state['input']['style_options']['initial_point']['auto_zoom'] >= 20)){
        form_set_error('style_options][initial_point][auto_zoom',t('You must include a valid zoom value for the Default Zoom field'));
      }
    }else{
      if(!is_numeric($form_state['input']['style_options']['initial_point']['zoom']) ||
          ($form_state['input']['style_options']['initial_point']['zoom'] <=0 ||
          $form_state['input']['style_options']['initial_point']['zoom'] >= 20)){
        form_set_error('style_options][initial_point][zoom',t('You must include a valid zoom value for the Zoom field'));
      }
      if(!is_numeric($form_state['input']['style_options']['initial_point']['latitude']) ||
          ($form_state['input']['style_options']['initial_point']['latitude'] <=0 ||
          $form_state['input']['style_options']['initial_point']['latitude'] >= 20)){
        form_set_error('style_options][initial_point][latitude',t('You must include a valid zoom value for the Latitude field. It must be between -90 and 90.'));
      }
      if(!is_numeric($form_state['input']['style_options']['initial_point']['longitude']) ||
          ($form_state['input']['style_options']['initial_point']['longitude'] <=0 ||
          $form_state['input']['style_options']['initial_point']['longitude'] >= 20)){
        form_set_error('style_options][initial_point][longitude',t('You must include a valid zoom value for the Longitude field. It must be between -180 and 180.'));
      }
    }

  }
  
  function render() {
    if ($this->view->preview == TRUE) {
      return '<div class="messages error">Preview is disabled for the Mapstraction style plugin.</div>';
    }
    else {
      // Would like to use parent::render() here but there seems to be problem
      // with the way the theme function is handled. Copying this from
      // views_plugin_style.inc for now.
      if ($this->uses_row_plugin() && empty($this->row_plugin)) {
        vpr('views_plugin_style_default: Missing row plugin');
        return;
      }

      // Group the rows according to the grouping field, if specified.
      $sets = $this->render_grouping($this->view->result, $this->options['grouping']);

      // Render each group separately and concatenate.  Plugins may override this
      // method if they wish some other way of handling grouping.
      $output = '';
      foreach ($sets as $title => $records) {
        if ($this->uses_row_plugin()) {
          $rows = array();
          foreach ($records as $label => $row) {
            $rows[] = $this->row_plugin->render($row);
          }
        }
        else {
          $rows = $records;
        }

        $output .= theme($this->theme_functions(), $this->view, $this->options, $rows, $title);
      }
      return $output;
    }
  }
  
  function map_features($rows) {
    $features = array();
    foreach ($this->options['fields']['wkt'] as $field_name => $selected){
      if(!empty($selected)){
        $data_set[$field_name]['wkt'] = array();
        $data = mapstraction_cck_feature_data(content_fields(substr($field_name,0,-4)));
        $data_set[$field_name]['data'] = $data['feature'];
      }
    }
    foreach ($rows as $id => $row) {
      if(!isset($features[$row->nid])){
        $features[$row->nid]['fields'] = $data_set;
      }
      foreach ($this->view->field as $key => $field) {
        if (!isset($processed[$row->nid][$field->field])&& $field->options['exclude'] == 0){
          $features[$row->nid]['bubble'] .= $field->theme($row).'<br>';
          $processed[$row->nid][$field->field] = 1;
        }

        foreach ($this->options['fields']['wkt'] as $field_name){
          if ($key == $this->options['fields']['wkt'][$field_name] && !in_array($field->theme($row),$features[$row->nid]['fields'][$field_name]['wkt'])){
            $field_value = $field->theme($row);
            if(!empty($field_value))$features[$row->nid]['fields'][$field_name]['wkt'][] = $field_value;
          }
        }
      }
    }
    return $features;
  }
}