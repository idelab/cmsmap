<?php
// $Id$
/* @file
 * mapstraction-views-map.tpl.php
 *
 * this file is all about getting theme preprocess functionality.
 *
 * geocoding widget
 *   $geocode
 * div attributes
 *   $width
 *   $height
 *   $id
 */
?>
<?php print $geocode?>
<div id="<?php print $id; ?>" class="mapstraction-map" style="width: <?php print $width; ?>px; height: <?php print $height; ?>px;">
</div>