// $Id$

/**
 * Global Object for Namespace
 */
var Mapstraction = Mapstraction || {};

/**
 * Onload page event which starts the map proccessing
 */
jQuery(document).ready(function() {
  
  for (var mapid in Drupal.settings.mapstraction_views_formatter.maps){
	if(!Mapstraction.isSet(Mapstraction.maps[mapid]))continue;
	var map = Drupal.settings.mapstraction_views_formatter.maps[mapid];
	var MapstractionMap = Mapstraction.maps[mapid].map
	var wktFormat = new OpenLayers.Format.WKT();
	for (var i in map.nodes){
	  for (var j in map.nodes[i].fields){
	    for (var k in map.nodes[i].fields[j].wkt){
	      // read the wkt values into an OpenLayers geometry object
	      var newFeature = wktFormat.read(map.nodes[i].fields[j].wkt[k]);
	      var data = map.nodes[i].fields[j].data;
	      if(Mapstraction.isSet(newFeature))
	      	if(data === null){
	          data = {};	      	
			}
	        data.infoBubble = map.nodes[i].bubble;	
	  	    Mapstraction.addFeature(newFeature,MapstractionMap,data)
	    }
	  }
	}
	// Set auto center and zoom.
    if (map.initialPoint.auto) {
      Mapstraction.centerMap(MapstractionMap, new mxn.BoundingBox(-90,-180,90,180),Number(map.initialPoint.auto_zoom));
    }
    else {
      var startPoint = new mxn.LatLonPoint(Number(map.initialPoint.latitude), Number(map.initialPoint.longitude));
      MapstractionMap.setCenterAndZoom(startPoint, Number(map.initialPoint.zoom));
    }
  }  
});


