// $Id$ 

Mapstraction.geocoder = Mapstraction.geocoder || {};

Drupal.behaviors.openlayers_geocoder = function (context) {

  if (Drupal.jsAC) {
  /**
   * Hides the autocomplete suggestions
   */
  Drupal.jsAC.prototype.hidePopup = function (keycode) {
    // Select item if the right key or mousebutton was pressed
    if (this.selected && ((keycode && keycode != 46 && keycode != 8 && keycode != 27) || !keycode)) {
      this.input.value = this.selected.autocompleteValue;
    }
    // Hide popup
    var popup = this.popup;
    if (popup) {
      this.popup = null;
      $(popup).fadeOut('fast', function() { $(popup).remove(); });
      // Add-on for Mapstraction CCK Geocoder module
      if ($(this.input).attr('geoautocomplete')&& this.input.value != '') {
        geocoder = new Drupal.Geocoder(this);
        geocoder.process(this.input.value);
      }
    }
    this.selected = false;
    
  };
  
  /**
   * Performs a cached and delayed search
  */
  Drupal.ACDB.prototype.search = function (searchString) {
    var db = this;
    this.searchString = searchString;

    // See if this key has been searched for before
    if (this.cache[searchString]) {
      return this.owner.found(this.cache[searchString]);
    }

    // Initiate delayed search
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(function() {
      db.owner.setStatus('begin');

      // Ajax GET request for autocompletion
      $.ajax({
        type: "GET",
        url: db.uri +'/'+ Drupal.encodeURIComponent(searchString),
        dataType: 'json',
        success: function (matches) {

          if ((typeof matches['status'] == 'undefined' || matches['status'] != 0) && matches.length != 0) {
            db.cache[searchString] = matches;
            // Verify if these are still the matches the user wants to see
            if (db.searchString == searchString) {
              db.owner.found(matches);
            }
            db.owner.setStatus('found');
          }
        },
        error: function (xmlhttp) {
          alert(Drupal.ahahError(xmlhttp, db.uri));
        }
      });
    }, this.delay);
  };
  
  }
};

Mapstraction.geocoder.callback = function(geocoded_location){
  Mapstraction.maps[Mapstraction.geocoder.mapid].map.setCenterAndZoom(geocoded_location.point,
  	parseInt(Mapstraction.geocoder.maps[Mapstraction.geocoder.mapid].geocode.zoom));
}

/**
 * Geocoder object
 */
Drupal.Geocoder = function (data) {
  this.data = data;
};

/**
 * Performs a search
 */
Drupal.Geocoder.prototype.process = function (query) {
  var mapid = $(this.data.input).attr('rel');
  Mapstraction.geocoder.component.swap(Mapstraction.geocoder.maps[mapid].geocode.provider);
  var address = {}; 
  address.address = query;
  Mapstraction.geocoder.mapid = mapid;
  Mapstraction.geocoder.component.geocode(address);
}

jQuery(document).ready(function(){
	if(Mapstraction.isSet(Drupal.settings.mapstraction)){
		Mapstraction.geocoder.maps = Drupal.settings.mapstraction.maps
		for (var mapid in Mapstraction.geocoder.maps) {
			if(Mapstraction.isSet(Mapstraction.geocoder.maps[mapid].geocode) && Mapstraction.geocoder.maps[mapid].geocode.provider != 'none'){
		      if(!Mapstraction.isSet(Mapstraction.geocoder.component)){
		      	Mapstraction.geocoder.component = new mxn.MapstractionGeocoder(Mapstraction.geocoder.callback,Mapstraction.geocoder.maps[mapid].geocode.provider,function(){});
		      }	
		      $('#mapstraction-cck-geocoder-'+mapid+'-field').keypress(function(e){
		      	if(e.which == 13){
		      		var mapid = $(this).attr('rel');	
		      		$('#mapstraction-cck-geocoder'+mapid+'-button').click()
		      		e.preventDefault();	}
		      })
		      // Define click actions for Geocode button
		      $('#mapstraction-cck-geocoder'+mapid+'-button').click(function() {
		        var mapid = $(this).attr('rel');
		        Mapstraction.geocoder.component.swap(Mapstraction.geocoder.maps[mapid].geocode.provider);
		        var address = {}; 
		        address.address = $('#mapstraction-cck-geocoder-'+mapid+'-field').val();
		        Mapstraction.geocoder.mapid = mapid;
		        Mapstraction.geocoder.component.geocode(address);
		        return false;
		      });
		    }	
		}
	}
})







