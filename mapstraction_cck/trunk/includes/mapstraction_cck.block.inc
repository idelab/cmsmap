<?php
// $Id: mapstraction_cck.block.inc,v 1.1 2011/01/11 17:31:37 plopesc Exp $ 

/**
 * @file
 * This file holds the functions for the main Mapstraction CCK Blocks.
 *
 * @ingroup Geo
 */

/**
 * Internal implementation of hook_block().
 * @param string $op
 * @param integer $delta
 * @param array $edit
 */
function mapstraction_cck_block_implementation($op = 'list', $delta = 0, $edit = array()){
  switch ($op){
    case 'list':
    $blocks[0] = array(
        'info' => t('Shows the page geographic info')
        , 'weight' => 0
        , 'status' => 1
        , 'cache' => BLOCK_NO_CACHE
        , 'region' => 'left');
    if($GLOBALS['db_type'] == 'pgsql' || mysqli_get_server_info($GLOBALS['active_db']) == '5.1.35-snapshot20090630'){    
      $blocks[1] = array(
          'info' => t('Shows the nearest nodes')
          , 'weight' => 0
          , 'status' => 1
          , 'cache' => BLOCK_NO_CACHE
          , 'region' => 'left');
    }
    return $blocks;
     
    case 'configure':
    switch ($delta){
      case 0:
      $form['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Title'),
        '#default_value' => variable_get('mapstraction_cck_geo_block_title', ''),
      );
      $form['provider'] = array(
        '#type' => 'select',
        '#title' => t('Available providers'),
        '#description' => t('Select the Map provider you want to show in the block'),
        '#options' => mapstraction_cck_available_providers(),
        '#default_value' => variable_get('mapstraction_cck_geo_block_provider', ''),
      );
      $form['controls'] = array(
        '#type' => 'checkbox',
        '#title' => t('Show controls'),
        '#description' => t('Select whether controls are displayed on the map or not'),
        '#return_value' => 1, 
        '#default_value' => variable_get('mapstraction_cck_geo_block_controls', 0),
      );
      $form['bubbles'] = array(
        '#type' => 'checkbox',
        '#title' => t('Show information bubbles'),
        '#description' => t('Select whether bubbles with link to node are displayed on the map markers or not'),
        '#return_value' => 1, 
        '#default_value' => variable_get('mapstraction_cck_geo_block_bubbles', 0),
      );
      $form['centroid'] = array(
        '#type' => 'checkbox',
        '#title' => t('Show markers associated to polylines\' centroid'),
        '#description' => t('Select whether a marker placed on the polylines\'s centroid is displayed or not'),
        '#return_value' => 1, 
        '#default_value' => variable_get('mapstraction_cck_geo_block_bubbles', 0),
      );
      break;
      case 1:
      $form['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Title'),
        '#default_value' => variable_get('mapstraction_cck_distance_block_title', ''),
      );
      $form['units'] = array(
        '#type' => 'radios',
        '#title' => t('Geographical units'),
        '#description' => t('Select the units you want to use in the block'),
        '#options' => geo_units(),
        '#default_value' => variable_get('mapstraction_cck_distance_block_units', 'km'),
      );
      $form['distance'] = array(
        '#type' => 'textfield',
        '#title' => t('Distance'),
        '#description' =>t('Select the max distance you want to filter.'),
        '#default_value' => variable_get('mapstraction_cck_distance_block_distance', 5),
        '#required' => true,
        '#element_validate' => array('mapstraction_cck_distance_block_validate'),
      );
      $form['limit'] = array(
        '#type' => 'textfield',
        '#title' => t('Max number of nodes'),
        '#maxlength' => 2,
        '#size' => 2,
        '#description' =>t('Select the max number of nodes you want to show. 0 or blank value for unlimited.'),
        '#default_value' => variable_get('mapstraction_cck_distance_block_limit', ''),        
        '#element_validate' => array('mapstraction_cck_distance_block_validate'),
      );
      break;
    }
    return $form;
    case 'save':
    switch ($delta){
      case 0:
      variable_set('mapstraction_cck_geo_block_provider', $edit['provider']);
      variable_set('mapstraction_cck_geo_block_title', $edit['title']);
      variable_set('mapstraction_cck_geo_block_controls', $edit['controls']);
      variable_set('mapstraction_cck_geo_block_bubbles', $edit['bubbles']);
      variable_set('mapstraction_cck_geo_block_centroid', $edit['centroid']);
      break;  
      case 1:
      variable_set('mapstraction_cck_distance_block_units', $edit['units']);
      variable_set('mapstraction_cck_distance_block_title', $edit['title']);
      variable_set('mapstraction_cck_distance_block_distance', $edit['distance']);
      variable_set('mapstraction_cck_distance_block_limit', $edit['limit']);
      break;  
    }
    break;
    case 'view':
    switch($delta) {
      case 0:
      $block = array(
      'subject' => variable_get('mapstraction_cck_geo_block_title', ''), 
      'content' => mapstraction_cck_geo_block_items());
      break;
      case 1:
      $block = array(
      'subject' => variable_get('mapstraction_cck_distance_block_title', ''), 
      'content' => mapstraction_cck_distance_block_items());
      break;
    }
    return $block;
  }
}
/**
 * Validation for the nearest nodes block form
 * @param $element
 *  Element to validate
 * @param $form_state
 *  Form state
 */
function mapstraction_cck_distance_block_validate($element, &$form_state){
   if($element['#name'] == 'distance'){
     if(floatval($element['#value']) <= 0)
       form_set_error('distance', 'Distance filter value must be numeric positive.');
   }else if($element['#name'] == 'limit'){
     if(intval($element['#value']) <= 0 && $element['#value'] != "" && $element['#value'] != '0')
       form_set_error('limit', 'Limit value must be integer.');
   }
}

/**
 * Implementation of page features block
 * @return theme for the block 
 */
function mapstraction_cck_geo_block_items(){
  if(!empty($GLOBALS['nodes'])){
    $field_prop = array();
    foreach($GLOBALS['nodes'] as $node){
      foreach(geo_field_names() as $field){
        if(isset($node->$field)){
          if(!isset($field_prop[$field])){
            $prop = mapstraction_cck_feature_data(content_fields($field));
            $a = geo_load(array('name' => $field));
            $field_prop[$field]['data'] = $prop['feature'];
            $field_prop[$field]['geo'] = geo_load(array('name' => $field));
          }
          foreach($node->$field as $feature){ 
            $wkt = geo_wkb_get_data($feature['wkb'],'wkt');
            if(isset($wkt)){              
              $items[] = array(
                'wkt' => $wkt['value'],
                'data' => $field_prop[$field]['data'],
                'node_info' => array('nid' => $node->nid,'title' => $node->title),
              ); 
            }
          }
        //If show marker associated to centroid checkbox is selected, the centroid is obtained and displayed in the map
          if(variable_get('mapstraction_cck_geo_block_centroid', 0) && $feature['gis type'] != 'point'){
            //This hard coded implementation should be replaced with GEO API functions as soon as possible
            if($feature['gis type'] == 'polygon'){
              $res = db_query("SELECT ASTEXT(CENTROID(ENVELOPE(".$field_prop[$field]['geo']->columnName()."))) AS wkt FROM ".$field_prop[$field]['geo']->tableName()." WHERE NID = $node->nid");
              
            }else{
              if($GLOBALS['db_type'] == 'pgsql'){
                $num = "CAST(CEIL(NUMPOINTS(".$field_prop[$field]['geo']->columnName().")/2) AS INTEGER)";
              }else{
                $num = "CEIL(NUMPOINTS(".$field_prop[$field]['geo']->columnName().")/2)";
              }
              $res = "SELECT ASTEXT(PointN(".$field_prop[$field]['geo']->columnName().",".$num."))) AS wkt FROM ".$field_prop[$field]['geo']->tableName()." WHERE NID = $node->nid";
              $res = db_query("SELECT ASTEXT(PointN(".$field_prop[$field]['geo']->columnName().",".$num.")) AS wkt FROM ".$field_prop[$field]['geo']->tableName()." WHERE NID = $node->nid");
            }
            while ($row = db_fetch_array($res)) {
              $items[] = array(
                'wkt' => $row['wkt'],
                'data' => $field_prop[$field]['data'],
                'node_info' => array('nid' => $node->nid,'title' => $node->title),
              );
            }
          } 
        }
      }
    }
    if (count($items)) {
      //If bubble with link checkbox is selected, the infoBubble is added to the marker
      if(variable_get('mapstraction_cck_geo_block_bubbles', 0) > 0){
        foreach($items as  $key => $item){
          $items[$key]['data']['infoBubble'] = l($items[$key]['node_info']['title'],'node/'.$items[$key]['node_info']['nid']);
          unset($items[$key]['nid']);
        }
      }
      // Build our map array
      $mapid = MAPSTRACTION_CCK_MAP_ID_PREFIX.'-geo-block';
      $mapstraction_single = array(
        'mapstraction_formatter' =>array(
          'maps' => array(
            $mapid => array(
              'features' => $items,
            )
          )
        )
      );
      drupal_add_js($mapstraction_single,'setting');
      $map_def['id'] =$mapid;
      $map_def['provider'] = variable_get('mapstraction_cck_geo_block_provider', NULL);
      $map_def['control'] = variable_get('mapstraction_cck_geo_block_controls', NULL) > 0 ? 'small':'none';
      $map_def['height'] = '200px';
      $map_def['width'] = '100%';
      $map = mapstraction_cck_render_map($map_def);
      drupal_add_js(drupal_get_path('module', 'mapstraction_cck') .'/js/mapstraction_cck.js');
      mapstraction_cck_load_maps($map['provider']);
      $mapstraction_single = array(
        'mapstraction_formatter' =>array(
          'maps' => array(
            $mapid => array(
              'bbox' => mapstraction_cck_default_bounds(),  
            )
          )
        )
      );
      drupal_add_js($mapstraction_single,'setting');
      return $map['themed'];
    }else return NULL;
  }else return NULL;      
}

/**
 * Implementation of nearest nodes block
 * @return theme for the block 
 */
function mapstraction_cck_distance_block_items(){
  if(!empty($GLOBALS['nodes'])&& ($GLOBALS['db_type']== 'pgsql'|| mysqli_get_server_info($GLOBALS['active_db']) == '5.1.35-snapshot20090630')){
    $field_prop = array();
    $geom = 'GEOMETRYCOLLECTION(';
    $page_nodes = "";
    foreach($GLOBALS['nodes'] as $nid => $node){
      $page_nodes.=$nid.',';
      foreach(geo_field_names() as $field){
        if(isset($node->$field)){
          foreach($node->$field as $feature){
            $wkt = geo_wkb_get_data($feature['wkb'],'wkt');
            if(isset($wkt)){
              $geom.=$wkt['value'].',';
            }
          }
        }
      }
    }
    if (strlen($geom) > 20) {
      if($GLOBALS['db_type'] != 'pgsql'){
        $geom = substr($geom,0,-1).')';
        $page_nodes = substr($page_nodes,0,-1);
        $result = db_query("SELECT X(CENTROID(ENVELOPE(GEOMFROMTEXT('%s')))) as lon, Y(CENTROID(ENVELOPE(GEOMFROMTEXT('%s')))) AS lat" ,$geom,$geom);
        while ($center = db_fetch_array($result)) {
          $centroid['lat'] = floatval($center['lat']);
          $centroid['lon'] = floatval($center['lon']);    
        }
        $distance = geo_unit_convert(variable_get('mapstraction_cck_distance_block_distance', 5),variable_get('mapstraction_cck_distance_block_units', 'km'),'m');
        $projected_distance = $distance*180/(6370986*pi());
        $nodes = array();
        foreach(geo('tables')as $table => $geo_field){
          $result = db_query("SELECT node.title, node.nid, min(distance(geo.%s,geomfromtext('%s'))) AS distance
        FROM {%s} geo,{node} node
          WHERE distance(geo.%s,geomfromtext('%s'))<%d
          AND geo.nid NOT IN (%s) AND geo.nid = node.nid
            GROUP BY node.title,node.nid
            order by distance",$geo_field[0],$geom,$table,$geo_field[0],$geom,$projected_distance,$page_nodes);
          while ($node = db_fetch_array($result)) {
            if(!isset($nodes[$node['nid']])||$nodes[$node['nid']]['distance'] > $node['distance']){ 
              $nodes[$node['nid']]['distance'] = $node['distance'];
              $nodes[$node['nid']]['title'] = $node['title'];
              $nodes[$node['nid']]['nid'] = $node['nid'];
            }
          }
        }
        foreach($nodes as $ind=>$node){
      $distance_vert = 2*6370986*asin(sqrt(pow(sin($node['distance']/(2*sqrt(2))*pi()/180),2)*(1+cos($centroid['lat']*pi()/180)*cos(($centroid['lat']+$node['distance']/sqrt(2))*pi()/180))));
          if($distance_vert>$distance){
        unset($nodes[$ind]);
      }else{
        $nodes[$ind]['distance'] = 2*6370986*asin(sqrt(pow(sin($node['distance']/(2*sqrt(2))*pi()/180),2)*(1+cos($centroid['lat']*pi()/180)*cos(($centroid['lat']+$node['distance']/sqrt(2))*pi()/180))));
          }
    } 
        
      }else{
        $geom = substr($geom,0,-1).')';
        $page_nodes = substr($page_nodes,0,-1);
        $distance = geo_unit_convert(variable_get('mapstraction_cck_distance_block_distance', 5),variable_get('mapstraction_cck_distance_block_units', 'km'),'m');
        $nodes = array();
        foreach(geo('tables')as $table => $geo_field){
          $result = db_query("SELECT node.title, node.nid, min(distance(transform(geo.%s,2163),transform(ST_geomfromtext('%s',4326),2163))) AS DISTANCE
        FROM {%s} geo,{node} node
          WHERE ST_DWithin(transform(geo.%s,2163),transform(ST_geomfromtext('%s',4326),2163), %s)
          AND geo.nid NOT IN (%s) AND geo.nid = node.nid
            GROUP BY node.title,node.nid
            order by distance",$geo_field[0],$geom,$table,$geo_field[0],$geom,$distance,$page_nodes);
          while ($node = db_fetch_array($result)) {
            if(!isset($nodes[$node['nid']])||$nodes[$node['nid']]['distance'] > $node['distance']){ 
              $nodes[$node['nid']]['distance'] = $node['distance'];
              $nodes[$node['nid']]['title'] = $node['title'];
              $nodes[$node['nid']]['nid'] = $node['nid'];
            }
          }
        }
      }
      if(count($nodes)>0){
        $return = '<table class = "view-table" style = "width:100%;"><tr><th>'.t('node').'</th><th>'.t('distance').'</th></tr>';
        $class = 'odd';
        function cmp($a, $b){
          if ($a["distance"] == $b["distance"]) {
          return 0;
          }
          return ($a["distance"] <$b["distance"]) ? -1 : 1;
        }
        usort($nodes,cmp);
        if(variable_get('mapstraction_cck_distance_block_limit', 0) > 0){
          $nodes = array_slice($nodes,0,variable_get('mapstraction_cck_distance_block_limit', 0));
        } 
        foreach($nodes as $node => $props){
          $return.='<tr class = "'.$class.'"><td>'.l($props['title'],'node/'.$props['nid']).'</td><td>'.number_format(geo_unit_convert($props['distance'],'m',variable_get('mapstraction_cck_distance_block_units', 'km')),2,'.','').' '.variable_get('mapstraction_cck_distance_block_units', 'km').'</td></tr>';
          $class == 'odd' ? $class = 'even': $class = 'odd';
        }
        return $return.='</table>';
      }else return NULL;
    }else return NULL;
  }else return NULL;      
}