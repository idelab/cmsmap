<?php
// $Id: mapstraction_cck.theme.inc,v 1.1 2011/01/11 17:31:37 plopesc Exp $ 

/**
 * @file
 * This file holds the functions for the Mapstraction CCK themes.
 *
 * @ingroup Geo
 */

/**
 * Theme function for mapstraction_map
 */
function theme_mapstraction_generic_map($map = array()) {
  $output = '
    <div id="'. $map['id'] .'" class="mapstraction-map" style="width:400px;height:300px;"></div>
  ';
  return $output;
}

/**
 * Theme function for mapstraction_cck_geo_field element
 */
function theme_mapstraction_cck_geo_field($element) {
  return $element['#children'];
}

/**
 * Theme the mapstraction_cck_picker widget.
 */
function theme_mapstraction_cck_picker($element) {
  return $element['#children'];
}

/**
 * Theme function for mapstraction_cck_map
 */
function theme_mapstraction_cck_map($field = array(), $map = array()) {  
  $output = '
    <div class="form-item mapstraction-cck-widget-container">
      <label for="'. $map['id'] .'">'. $field['widget']['label'] .'</label>
      <div id="'.$map['id'].'-count-msg" class="messages error" style="display:none">
      '.t('You can\'t add more features to the map').'
      </div>
      <div class="mapstraction-cck-controls">
        <div id="'. $map['id'] .'-add-feature" class="mapstraction-cck-add-feature-control-active" rel="'. $map['id'] .'">'. t('Add new Feature') .'</div>'.
        addEditionControl($field, $map).addClearTagsControl($field, $map).
    '</div>
      '.$map['geocode_theme'].$map['themed'].'
      <div class="description mapstraction-cck-field-description">
        '. $field['widget']['description'] .'
      </div>
      <div class="description mapstraction-cck-map-instructions">'.$map['instructions'].'</div>
      <div class="mapstraction-cck-actions">
        <a href="#" id="'. $map['id'] .'-wkt-switcher" rel="'. $map['id'] .'">'. t('Show/Hide WKT Fields') .'</a>
      </div>
    </div>
  ';
  return $output;
}

/**
 *  Function that includes the Activate/Deactivate editing mode control if it is enabled
 * @param $field
 * @param $map
 */

function addEditionControl($field, $map){
  if(!empty($field['widget']['mapstraction_cck']['controls']['show_edition']))return '<div id="'. $map['id'] .'-activate-edition" class="mapstraction-cck-edition-control-active" rel="'. $map['id'] .'" active="false" edition-active="'.t('Deactivate Editing').'" edition-inactive="'.t('Activate Editing').'">'. t('Activate Editing') .'</div>';
  else return "";
}

/**
 *  Function that includes the Clear Geotags control if it is enabled
 * @param $field
 * @param $map
 */

function addClearTagsControl($field, $map){
  if(!empty($field['widget']['mapstraction_cck']['controls']['clear_tags']))return '<div id="'. $map['id'] .'-clear-tags" class="mapstraction-cck-clear-tags-control-active" rel="'. $map['id'] .'">'. t('Clear map') .'</div>';
  else return "";
}

/**
 * Theme function for mapstraction_cck_config_map
 */
function theme_mapstraction_cck_config_map($field = array(), $map = array()) {  
  $aux = '';
  foreach ($map['providers'] as $key => $value) {
    $aux.='<div id="config-'. $key .'" style="width:'.$map['width'].';height:'.$map['height'].';position:relative;display:none;"></div>';
  } 
  $output = '
    <div class="form-item mapstraction-cck-config-map-container">
      <label for="config">'.t('Configure the map:').'</label>
      
      '.$aux.'
      <div class="description mapstraction-cck-field-description">
        '. $field['widget']['description'] .'
      </div>
      <div class="description mapstraction-cck-map-instructions">'.t('Pan the map and select your own Bounding Box. This will be the widget default Bounding Box. In the map features you can see how it will look in the map in real time.').'</div>
    </div>
  ';
  return $output;
}

/**
 * Theme a fields using a map. Each field is given a map.
 */
function theme_mapstraction_cck_formatter_mapstraction_cckmapformatter_single($element) { 
  
  // Define some common variables
  $node = $element['#node'];
  $geofield_key = $element['#field_name'];
  $mapid = "mapstraction-cck-field-". $node->nid ."-single-". $geofield_key;
  
  // Name our features. If we are in a view, name the feature by the node name.
  // If we are in a page, name the feature by the field name. This is necessary
  // because the tricky $element changes it's form depending on context.
  if ($element['#title']){
    $feature_name = $element['#title'];
  }
  elseif ($node->content[$geofield_key]['field']['#title']){
    $feature_name = $node->content[$geofield_key]['field']['#title'];
  }
  else {
    $feature_name = $node->node_title;
  }
  
  if(features_to_formatter($element,$mapid)){
    return mapstraction_cck_map_formatter(content_fields($element['#field_name']),$mapid);
  }else{
    return FALSE;
  }
}


/**
 * Theme a fields using a map. Every fields are showed in a map.
 */
function theme_mapstraction_cck_formatter_mapstraction_cckmapformatter_grouped($element) { 

  static $processed = FALSE;
  static $mapid ='';
  if($processed != $element['#node']){
    $processed = $element['#node'];
  // Define some common variables
  $node = $element['#node'];
  $geofield_key = $element['#field_name'];
  $mapid = "mapstraction-cck-field-". $node->nid ."-grouped";
  
  // Name our features. If we are in a view, name the feature by the node name.
  // If we are in a page, name the feature by the field name. This is necessary
  // because the tricky $element changes it's form depending on context.
  if ($element['#title']){
    $feature_name = $element['#title'];
  }
  elseif ($node->content[$geofield_key]['field']['#title']){
    $feature_name = $node->content[$geofield_key]['field']['#title'];
  }
  else {
    $feature_name = $node->node_title;
  }

  if(features_to_formatter($element,$mapid)){
    return mapstraction_cck_map_formatter(content_fields($element['#field_name']),$mapid);
  }else{
    return FALSE;
  }
  }else{
     features_to_formatter($element,$mapid);
     return FALSE;
  }
  
}
/**
 * Gets the features from an element and push it in array that will be passed to the map.
 * @param $element
 * @param $mapid
 * @return boolean 
 *  TRUE if there are features in the field, else FALSE
 */
function features_to_formatter($element,$mapid){
   module_load_include('module', 'geo', 'geo');
  
   $field = content_fields($element['#field_name']);
    $data = mapstraction_cck_feature_data($field);
      $data['bbox'] = $field['widget']['mapstraction_cck']['bbox'];
   // Add features to the map definition
  
   foreach ($element  as $key => $feature) {
    if (is_numeric($key)){
      $wkt = geo_wkb_get_data($feature['#item']['wkb'],'wkt');
      if(isset($wkt)){
        $items[] = array(
          'wkt' => $wkt['value'],
          'data' => $data['feature'],
        );
      }
    }
  }
  
  // If there are any features to display, then display the map
  if (count($items)) {
    // Build our map array
   
    $mapstraction_single = array(
      'mapstraction_formatter' =>array(
        'maps' => array(
          $mapid => array(
            'features' => $items,
          )
        )
      )
    );
    drupal_add_js($mapstraction_single,'setting');
    return TRUE;
  }
  else return FALSE;
}
/**
 * Adds the map for field formatters
 * @param $field
 *  field to extract features
 * @param $mapid
 *  map to features will be added
 * @return theme for the map
 */
function mapstraction_cck_map_formatter($field,$mapid){
    $map_def['id'] =$mapid;
      $map_def['provider'] = $field['widget']['mapstraction_cck']['provider'];
      $map_def['control'] = $field['widget']['mapstraction_cck']['control'];
      $map_def['type'] = $field['widget']['mapstraction_cck']['map_type'];
      $map_def['default_zoom'] = $field['widget']['mapstraction_cck']['default_zoom'];
      $map = mapstraction_cck_render_map($map_def);
      drupal_add_js(drupal_get_path('module', 'mapstraction_cck') .'/js/mapstraction_cck.js');
      mapstraction_cck_load_maps($map['provider']);
      if(!isset($field['widget']['mapstraction_cck']['bbox'])){$bbox = mapstraction_cck_default_bounds();
      }else $bbox = $field['widget']['mapstraction_cck']['bbox'];
      $mapstraction_single = array(
      'mapstraction_formatter' =>array(
        'maps' => array(
          $mapid => array(
            'bbox' => $bbox,  
          )
        )
      )
    );
    drupal_add_js($mapstraction_single,'setting');
    return $map['themed'];
}