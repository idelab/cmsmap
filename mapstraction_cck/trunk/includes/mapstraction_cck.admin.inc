<?php
// $Id: mapstraction_cck.admin.inc,v 1.1 2011/01/11 17:31:37 plopesc Exp $ 

/**
 * @file
 * This file holds the functions for the main Mapstraction CCK Admin settings.
 *
 * @ingroup Geo
 */

/**
 * Configuration page form
 * @return array
 *  Array containing configuration module form fields
 */
function mapstraction_cck_admin() {
  // Source description
  $mapstraction_source_description = t('The source for the IDELab Mapstraction library can be one of two things:')
    .'<ul><li>'
    . t('URL: This means that the IDELab Mapstraction JS library is not hosted on this site. IDELab Mapstraction provides a hosted JS file.  By default the Mapstraction CCK module will use this, since the JS file cannot be included with the module.  This is @ol_api_url.  This may effect performance as it is not hosted on your site. ', array('@ol_api_url' => 'http://mvn.idelab.uva.es/idelabmapstraction/downloads/idelabmapstraction.js'))
    .'</li><li>'
    . t('Drupal Path: This is a path relative to the Drupal base.  For instance, if you <a href="!ol_url">Download IDELABMapstraction</a> manually to the Mapstraction CCK module folder and extract it, you may use a value like: @suggested_path',
      array(
        '!ol_url' => 'http://dev.idelab.uva.es/idelabmapstraction/',
        '@suggested_path' => drupal_get_path('module', 'mapstraction_cck') . '/source',
      )
    );
  $openlayers_source_description = t('The source for the OpenLayers library can be one of two things:')
    . '<ul><li>'
    . t('URL: This means that the OpenLayers JS library is not hosted on this site. OpenLayers provides a hosted JS file. By default the Open Layers module will use this, since the JS file cannot be included with the module.  This is @ol_api_url. This may effect performance as it is not hosted on your site. Also, depending on how the OpenLayers JS API changes, this module may not be compatible with this version.', array('@ol_api_url' => 'http://openlayers.org/api/OpenLayers.js'))
    . '</li><li>'
    . t('Drupal Path: This is a path relative to the Drupal base. For instance, if you <a href="!ol_url">Download OpenLayers</a> manually to the OpenLayers module folder and extract it, you may use a value like: @suggested_path', 
      array(
        '!ol_url' => 'http://openlayers.org/',
        '@suggested_path' => drupal_get_path('module', 'mapstraction_cck') . '/source',
      )
    )
    . '</li></ul>';
  $form['mapstraction_cck_mapstraction_folder'] = array(
    '#type' => 'textfield',
    '#title' => t('IDELab Mapstraction Library folder'),
    '#default_value' => variable_get('mapstraction_cck_mapstraction_folder', "http://mvn.idelab.uva.es/idelabmapstraction/downloads/idelabmapstraction.js"),
    '#size' => 50,
    '#maxlength' => 255,
    '#required' => true,
    '#description' => $mapstraction_source_description,
  );
  
  $form['mapstraction_cck_openlayers_folder'] = array(
    '#type' => 'textfield',
    '#title' => t('OpenLayers Library folder'),
    '#default_value' => variable_get('mapstraction_cck_openlayers_folder', "http://openlayers.org/api/OpenLayers.js"),
    '#size' => 50,
    '#maxlength' => 255,
    '#required' => true,
    '#description' => $openlayers_source_description,
  );
  
  $form['mapstraction_cck_gm_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Maps API Key'),
    '#default_value' => variable_get('mapstraction_cck_gm_key', variable_get('googlemap_api_key', '')),
    '#size' => 50,
    '#maxlength' => 255,
    '#description' => t("Your personal Google Maps API key.  You must get this for each separate website at <a href='http://www.google.com/apis/maps/'>Google Map API website</a>."),
  );
  
  $form['mapstraction_cck_ym_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Yahoo! Maps API Key'),
    '#default_value' => variable_get('mapstraction_cck_ym_key',''),
    '#size' => 50,
    '#maxlength' => 255,
    '#description' => t("Your personal Yahoo! Maps API key.  You must get this for each separate website at <a href='http://developer.yahoo.com/maps/'>Yahoo! Maps API website</a>."),
  );
  $providers_selected =  mapstraction_cck_available_providers();

  foreach($providers_selected as $key=>$value){
    $aux[] = $key;
  }
  $form['mapstraction_cck_providers'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Providers'),
        '#description' => t('Choose the providers that can be used in the application.'),
        '#options' => mapstraction_cck_providers(),
        '#required' => TRUE,
        '#default_value' => $aux ,
        '#element_validate' => array('mapstraction_cck_providers_validate'),
      );
  $form['mapstraction_cck_default_provider'] = array(
        '#type' => 'select',
        '#title' => t('Default provider'),
        '#description' => t('Choose the default provider that will be used in the application.'),
        '#options' => mapstraction_cck_providers(),
        '#required' => TRUE,
        '#default_value' => variable_get('mapstraction_cck_default_provider','openlayers'),
        '#element_validate' => array('mapstraction_cck_default_provider_validate'),
      );
      

  return system_settings_form($form);
}

/**
 * Configuration page providers field validation
 * @param $element
 *  Element to validate
 * @param $form_state
 *  Form state
 */
function mapstraction_cck_providers_validate($element, &$form_state){
   if($form_state['values']['mapstraction_cck_gm_key']==""&&is_string($form_state['values']['mapstraction_cck_providers']['google'])){
     form_error($element['google'], t('Google Maps API key is required for Google Maps provider.'));
   }
   if($form_state['values']['mapstraction_cck_ym_key']==""&&is_string($form_state['values']['mapstraction_cck_providers']['yahoo'])){
     form_error($element['yahoo'], t('Yahoo! Maps API key is required for Yahoo! Maps provider.'));
   }
}

/**
 * Configuration page default provider field validation
 * @param $element
 *  Element to validate
 * @param $form_state
 *  Form state
 */
function mapstraction_cck_default_provider_validate($element, &$form_state){
   $providers = $form_state['values']['mapstraction_cck_providers'];
   if(is_numeric($providers[$form_state['values']['mapstraction_cck_default_provider']]))
     form_error($element, t('You can set as default a non selected provider'));
}
