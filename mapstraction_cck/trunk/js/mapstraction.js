// $Id$ 
var Mapstraction = Mapstraction || {};
Mapstraction.maps = Mapstraction.maps || {};
Mapstraction.mapDefs = Mapstraction.mapDefs || {};

/**
 * Mapstraction Base Drupal Behavoirs
 */
Drupal.behaviors.mapstraction = function(context){
	if ((typeof(Drupal.settings.mapstraction) == 'object') && (Mapstraction.isSet(Drupal.settings.mapstraction.maps))) {
    Mapstraction.loadMaps();
  }
}

/**
 * Check if Variable is define
 *
 * @params variable
 *   Any variable
 * @return
 *   Boolean if the variable is definied or not
 */
Mapstraction.isSet = function(variable) {
  if (typeof(variable) == 'undefined') {
    return false;
  }
  else {
    return true;
  }
}
/**
 * Checks if maps has been aleady loaded or not. If has not been rendered yet, calls to renderMap() 
 */
Mapstraction.loadMaps = function(){
	var mapDefs = Drupal.settings.mapstraction.maps;
	for (var i in mapDefs){
	  // Create a temp map variable 
	  var map = mapDefs[i];
	    
	  // Check if map is already rendered
	  if (Mapstraction.isSet(Mapstraction.maps[map.id]) && Mapstraction.isSet(Mapstraction.maps[map.id].rendered) && (Mapstraction.maps[map.id].rendered == true)) {
	    continue;
      }
      if ($('#' + map.id).length > 0) {
	    // Make div the right dimensions and add custom controls
	    $('#' + map.id).css('width', map.width).css('height', map.height).css('position','relative').css('display','block');
	    //$('#' + map.id).after(Drupal.theme('mapControls', map.id, '400px'));
	      
	    // Set-up our registry of active OpenLayers javascript objects for this particular map.
	    Mapstraction.maps[map.id] = {};
	    // Set up places for us to store layers, controls, etc.
	    Mapstraction.maps[map.id].active = false;
	    
	    Mapstraction.mapDefs[i] = mapDefs[i];
		
	    // Render Map
	    Mapstraction.renderMap(mapDefs[i]);
      }
      
	} 
}

/**
 * Render a Mapstraction Map
 * @param map
 * 	The map properties of map 
 */
Mapstraction.renderMap = function(map){

  Mapstraction.maps[map.id].map = new mxn.MapstractionInteractive(map.id,map.provider);
  Mapstraction.maps[map.id].map.setCenterAndZoom(new mxn.LatLonPoint(40,-3),5);
  Mapstraction.maps[map.id].map.mapid = map.id;
  //fire load event if it id defined
  if(Mapstraction.isSet(map.handlers)&&Mapstraction.isSet(map.handlers.load)){
  	Mapstraction.maps[map.id].map.load.addHandler(map.handlers.load);
  	Mapstraction.maps[map.id].map.load.fire();
  }
  //add geo_type attribute if it is necessary
  if(Mapstraction.isSet(map.geo_type))
  	Mapstraction.maps[map.id].map.geo_type = map.geo_type;
  if(Mapstraction.isSet(map.control)){
  	if (map.control == 'small')
  	  Mapstraction.maps[map.id].map.addSmallControls();
  	else if (map.control == 'large')
  	  Mapstraction.maps[map.id].map.addLargeControls();
  	else if (map.control == 'none')
  	  Mapstraction.maps[map.id].map.addControls({}); 
  }
  if(Mapstraction.isSet(map.type)&&parseInt(map.type)>0){
    Mapstraction.maps[map.id].map.setMapType(parseInt(map.type));
  } 
  // Mark as Rendered
  Mapstraction.maps[map.id].rendered = true;
  
}

/**
 * Center the map in function of their features, if there are not featues, shows the Bounding Box.
 * @param map
 * 	Map you want to automatic center
 * @param bbox
 * 	Default Bounding box if there are not features
 */
Mapstraction.centerMap = function(map,bbox,default_zoom){
  if((map.markers.length + map.polylines.length) > 1 )
  	map.autoCenterAndZoom();
  else{
  	map.setBounds(bbox);
  	if(map.markers.length == 1){
  	  map.setCenter(new mxn.LatLonPoint(map.markers[0].location.lat,map.markers[0].location.lon))
  	  if(default_zoom){
  		map.setZoom(parseInt(default_zoom));
  	  }
  	}
	if(map.polylines.length == 1){
  	  map.autoCenterAndZoom();
  	
  	  if(default_zoom){
  		if(map.getZoom()>parseInt(default_zoom)) map.setZoom(parseInt(default_zoom));
  	  } 
	}
  }
}

/**
 * Adds the specified feature to the map
 * @param feature
 * 	The feature you want to add to the map
 * @param map
 * 	The map you want to add the feature
 * @param data
 * 	Array with specific feature settings 
 */
Mapstraction.addFeature = function(feature,map,data){
	switch(feature.geometry.toString().substr(0,feature.geometry.toString().indexOf('(')).toLowerCase()){
	case "point":	
  	  var point = new mxn.LatLonPoint(feature.geometry.y,feature.geometry.x);
  	  var marker = new mxn.Marker(point);
  	  marker.setAttribute('drupalField',feature.drupalField);
  	  marker.addData(data);
  	  map.addMarker(marker,false,true);
	break;
	case "linestring":
	  var points = []
	  for (var i in feature.geometry.components)
	  	points.push(new mxn.LatLonPoint(feature.geometry.components[i].y,feature.geometry.components[i].x));
  	  var linestring = new mxn.Polyline(points);
  	  linestring.setAttribute('drupalField',feature.drupalField);
  	  linestring.addData(data);  	  
  	  map.addPolyline(linestring,false,true)
	break;
	case "polygon":
	  var points = []
	  for (var i in feature.geometry.components[0].components)
	  	points.push(new mxn.LatLonPoint(feature.geometry.components[0].components[i].y,feature.geometry.components[0].components[i].x));
  	  var polygon = new mxn.Polyline(points);
  	  polygon.setClosed(true);
  	  polygon.setAttribute('drupalField',feature.drupalField);
  	  polygon.addData(data);
  	  map.addPolyline(polygon,false,true)
	break;
	}
}
/**
 * Updates a feature from the map
 * @param feature
 * 	Feature you want to update
 * @param newFeature
 * 	Auxiliar feaure with the new feature coordinates
 */
Mapstraction.updateFeature = function(feature,newFeature){
	switch(newFeature.geometry.toString().substr(0,newFeature.geometry.toString().indexOf('(')).toLowerCase()){
	case "point":	
  	  feature.location = new mxn.LatLonPoint(newFeature.geometry.y,newFeature.geometry.x)
  	break;
  	case "linestring":
  	  var points = []
	  for (var i in newFeature.geometry.components)
	  	points.push(new mxn.LatLonPoint(newFeature.geometry.components[i].y,newFeature.geometry.components[i].x));
	  feature.points = points;
  	break;
  	case "polygon":
  	  var points = []
	  for (var i in newFeature.geometry.components[0].components)
	  	points.push(new mxn.LatLonPoint(newFeature.geometry.components[0].components[i].y,newFeature.geometry.components[0].components[i].x));
	  feature.points = points;
  	break;
	}
	feature.updateProprietary();
}

/**
 * Removes the feature from the map
 * @param feature
 * 	Feature to remove
 */
Mapstraction.removeFeature = function(feature){
	if(feature.proprietary_marker != null && Mapstraction.isSet(feature.proprietary_marker))
	  feature.mapstraction.removeMarker(feature);
	else{
	  if (Mapstraction.isSet(feature.markers)){
	  	for (var i in feature.markers)
	  	  feature.markers[i].mapstraction.removeMarker(feature.markers[i]);
	  }	
	  feature.mapstraction.removePolyline(feature);
	}
}

Mapstraction.addOverlay = function(map,georss){
	map.addOverlay(georss,true);
}
