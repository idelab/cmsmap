<?php
// $Id$ 
/**
 * @file
 * This file holds the main Drupal hook functions 
 * and private functions for the mapstraction_cck module.
 *
 * @ingroup Geo
 */

/**
 * Map ID Prefix
 */
define('MAPSTRACTION_CCK_MAP_ID_PREFIX', 'mapstraction-cck-map-auto-id');

/**
 * Load the functionality file
 */

module_load_include('inc', 'mapstraction_cck', 'includes/mapstraction_cck');

/**
 * Implementation of hook_help()
 */
function mapstraction_cck_help($path, $arg) {
  switch ($path) {
    case 'admin/help#mapstraction_cck':
      $output = '<p>'. t('The Mapstraction CCK module provides widgets and field formatters that interface with Mapstraction.') .'</p>';
    break;
  }
  return $output;
}

/**
 * Implementation of hook_theme().
 */
function mapstraction_cck_theme() {
  return array(
    'mapstraction_cck_geo_field' => array(
      'arguments' => array(
        'element' => NULL
      ),
      'file' => 'includes/mapstraction_cck.theme.inc',
    ),
    //for the new implementation::
    'mapstraction_cck_picker' => array(
      'arguments' => array('element' => NULL),
      'file' => 'includes/mapstraction_cck.theme.inc',
    ),  
    'mapstraction_cck_map' => array(
      'arguments' => array(
        'field' => NULL,
        'mapid' => NULL,
      ),
      'file' => 'includes/mapstraction_cck.theme.inc',
    ),
    'mapstraction_generic_map' => array(
      'arguments' => array(
        'field' => NULL,
        'mapid' => NULL,
      ),
      'file' => 'includes/mapstraction_cck.theme.inc',
    ),
    'mapstraction_cck_config_map' => array(
      'arguments' => array(
        'field' => NULL,
        'mapid' => NULL,
      ),
      'file' => 'includes/mapstraction_cck.theme.inc',
    ),
    'mapstraction_cck_formatter_mapstraction_cckmapformatter_single' => array(
      'arguments' => array('element' => NULL),
      'file' => 'includes/mapstraction_cck.theme.inc',
    ),   
    'mapstraction_cck_formatter_mapstraction_cckmapformatter_grouped' => array(
      'arguments' => array('element' => NULL),
      'file' => 'includes/mapstraction_cck.theme.inc',
    ),
  );
}

/**
 * Implementation of hook_menu()
 */
function mapstraction_cck_menu() {

  $items['admin/settings/mapstraction_cck'] = array(
    'title' => 'Mapstraction CCK',
    'description' => 'Select map providers you want to offer',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mapstraction_cck_admin'),
    'access arguments' => array('access administration pages'),
    'file' => 'includes/mapstraction_cck.admin.inc',
    'type' => MENU_NORMAL_ITEM,
   );

  return $items;
}

/**
 * Implementation of hook_gis_input_info().
 */
function mapstraction_cck_gis_input_info($gis_type = NULL) {
  // Note, we shou use the following so that we can actually control how
  // the widget will work, butdifficult to make unlimited work
  // and Geo does not support it yet
  // 'multiple values' => CONTENT_HANDLE_MODULE,
  $inputs = array(
    'mapstraction_cck_geo_field' => array(
      'label' => t('Mapstraction Map'),
      'gis input' => 'wkt',
      'safe reverse' => TRUE,
      'gis types' => array('point', 'linestring', 'polygon'),
      'element' => array(
        '#type' => 'mapstraction_cck_picker',
      ),
      'config' => 'mapstraction_cck_widget_settings',
      'settings callback' => array(
        'form' =>'mapstraction_cck_config_form',
        'validate' =>'mapstraction_cck_config_validate',
        'save' =>'mapstraction_cck_config_save',  
      ),
    ),
  );
  return $inputs;
}

/**
 * 
 * @param $widget the widget configuration array to load configuration values
 * @return $form configuration form for the mapstraction CCK widget
 */
function mapstraction_cck_config_form($widget){
  if (isset($widget['mapstraction_cck'])) {
    $defaults = $widget['mapstraction_cck'];
  }
  else {
    $provider = each(mapstraction_cck_available_providers());
    $defaults = array(
      'width' =>'100%',
      'height' => '400px',
      'bbox' =>mapstraction_cck_default_bounds(),    
      'provider' =>$provider['key'],
      'controls' => array(
        'default_edition' => 1,
        'show_edition' => 1,
        'clear_tags' => 1,   
        'clear_tags' => 1,
      ),
      'control' =>'small',
      'geocode' => array(
        'provider' => 'none',
        'zoom' => 10,),
      'map_type' => NULL,
      'default_zoom' => 12,
    );
  }

  // Get map form
  $map_form = mapstraction_cck_map_form($defaults);
  
  $form['mapstraction_cck'] = $map_form;
  $form['mapstraction_cck']['#type'] = 'fieldset';
  $form['mapstraction_cck']['#title'] = t('Map Settings');
  $form['mapstraction_cck']['#description'] = t('These are the settings that determine how the map will look.');
  $form['mapstraction_cck']['#collapsible'] = TRUE;
  $form['mapstraction_cck']['#collapsed'] = FALSE;
  if (!isset($form['mapstraction_cck']['#after_build'])) {
    $form['mapstraction_cck']['#after_build'] = array();
  }
  $form['mapstraction_cck']['#after_build'][] = 'geo_element_set_config_map';

  return $form;
}

/**
 * Validates the values introduced in the mapstraction cck widget config form
 * @param $widget widget configuration values introduced in the form
 */
function mapstraction_cck_config_validate($widget){
  mapstraction_cck_map_form_validate($widget['mapstraction_cck']);
}

/**
 * Saves the values introduced in the mapstraction cck widget config form
 * @param $widget widget configuration values introduced in the form
 */
function mapstraction_cck_config_save($widget){
  return array('mapstraction_cck');
}
 
/**
 * Implementation of hook_field_formatter_info(),.
 */
function mapstraction_cck_field_formatter_info() {
  return array(
    // @@TODO: Implement Grouped Formatter.
    'mapstraction_cckmapformatter_grouped' => array(
      'label' => t('Mapstraction CCK Grouped Map'),
      'field types' => array('geo'),
      'multiple values' => CONTENT_HANDLE_MODULE,
    ),
    'mapstraction_cckmapformatter_single' => array(
      'label' => t('Mapstraction CCK Single Map'),
      'field types' => array('geo'),
      'multiple values' => CONTENT_HANDLE_MODULE,
    ),
  );
}
/**
 * '#after_build' method for hook_widget() form.
 * Adds the map above the textareas
 * @param $form_element
 * @return $form_element
 */
function geo_element_set_config_map($form_element){   
      //add Map
      mapstraction_cck_load_maps(mapstraction_cck_available_providers());
      
      $map['providers'] = mapstraction_cck_available_providers();
      
      $map['id'] ='config-'.$form_element['provider']['#default_value'];
      $map['height'] = $form_element['height']['#default_value'];
      $map['width'] = $form_element['width']['#default_value'];
      $map['provider'] = $form_element['provider']['#default_value'];
      $map['control'] = $form_element['control']['#default_value'];
      $map['type'] = $form_element['map_type']['#default_value'];
      $map = mapstraction_cck_render_map($map); 

      $mapstraction_cck = array(
        'mapstraction_cck' => array(
          'maps' => array(
            $map['id'] => array(
              'field_map_themed' => theme('mapstraction_cck_config_map', $field, $map),
              'field_container' => 'edit-mapstraction-cck-provider-wrapper',  // Best way??
              'id' =>$map['id'],
      		  'provider' =>$widget['mapstraction_cck']['provider'],
              'bbox' =>$widget['mapstraction_cck']['bbox'],
            ),
          ),
        ),
      );
      
      drupal_add_js($mapstraction_cck,'setting');
      drupal_add_js(drupal_get_path('module', 'mapstraction_cck') .'/js/mapstraction_cck.form.js');
  return $form_element;
}

/**
 *'#after_build' method for hook_widget_settings() form.
 * Adds map to select BoundingBox and features configuration
 * @param $form_element
 * @return $form_element
 */
function geo_element_set_map($form_element){
  //TODO A�adir el campo desde aqu� en lugar de a pelo en HTML
  if($form_element['#delta'] != 0) return $form_element;
  $field = content_fields($form_element['#parents'][0]);
  if ($field == NULL) return $form_element;
  $fieldname = $field['field_name'];

  // Only include map once per field
  static $included_field = array();
  if (!in_array($fieldname,$included_field)) {
    // Create map
    $mapid = MAPSTRACTION_CCK_MAP_ID_PREFIX .'-'. $fieldname;
    $map['id'] = $mapid;
    if($field['widget']['mapstraction_cck']['geocode']['provider'] != 'none'){
      
    }
    
    $map['provider'] = $field['widget']['mapstraction_cck']['provider'];
    $map['height'] = $field['widget']['mapstraction_cck']['height'];
    $map['width'] = $field['widget']['mapstraction_cck']['width'];
    $map['control'] = $field['widget']['mapstraction_cck']['control'];
    $map['type'] = $field['widget']['mapstraction_cck']['map_type'];
    $map['default_zoom'] = $field['widget']['mapstraction_cck']['default_zoom'];
    $data = mapstraction_cck_feature_data($field);
    $map['data'] = $data['feature'];  
    $map['instructions'] = mapstraction_cck_use_map($map['provider']);
    
    // Add JS
    mapstraction_cck_load_maps($field['widget']['mapstraction_cck']['provider']);
    drupal_add_js(drupal_get_path('module', 'mapstraction_cck') .'/js/mapstraction_cck.js');
    
    //loading scripts for geocoding field
    if($field['widget']['mapstraction_cck']['geocode']['provider'] != 'none' && module_exists('mapstraction_cck_geocoder')){
      mapstraction_cck_geocoder_add_geocoder($map,$field['widget']['mapstraction_cck']['geocode']['provider']);
      $map['geocode'] = $field['widget']['mapstraction_cck']['geocode'];
    }
    
    //RENDER MAP
    $map = mapstraction_cck_render_map($map);
    if(intval($field['multiple']) == 0) $container = 'edit-'.str_replace('_', '-', $fieldname) .'-0-'.$field['type'].'-wkt-wrapper';
    else if(intval($field['multiple']) == 1) $container = str_replace('_', '-', $fieldname) .'-items';
    else if(intval($field['multiple']) > 1) $container = $fieldname .'_values';
    // Put together array for JS
    $mapstraction_cck = array(
      'mapstraction_cck' => array(
        'maps' => array(
          $mapid => array(
            'field_name' => $fieldname,
            'field_name_js' => str_replace('_', '-', $fieldname), // Best way?
            'field_map_themed' => theme('mapstraction_cck_map', $field, $map),
            'field_container' => $container,  // Best way??
            'multiple' => intval($field['multiple']),
            'bbox' =>isset($field['widget']['mapstraction_cck']['bbox'])? $field['widget']['mapstraction_cck']['bbox'] : mapstraction_cck_default_bounds(),
            'geo_type' => $field['geo_type'],
            'default_edition' => $field['widget']['mapstraction_cck']['controls']['default_edition'],
            'clear_tags_msg' => $field['widget']['mapstraction_cck']['controls']['clear_tags_msg'],
          ),
        ),
      ),
    );
    // Add JS settings
    drupal_add_js($mapstraction_cck, 'setting');
 
    // Change static variable
    $included_field[] = $fieldname;
  }
  return $form_element;
  }



/**
 * Impplementation of hook_form_form_id_alter().
 * Adds a info message in the Display fields screen
 * @param $form_id
 * @param $form
 */
function mapstraction_cck_form_content_display_overview_form_alter($form_id, &$form){

  $form_id['#fields'][] = 'description';
  $form_id['description'] = array(
    '#type' => 'item',
    '#value' => t("Mapstraction CCK Grouped Map display uses first field selected provider"),
    '#weight' => -1,
  );
}

/**
 * Implementation of hook_nodeapi()
 */
 function mapstraction_cck_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL){
   if($op == 'view'){
     if(!isset($GLOBALS['nodes']))$GLOBALS['nodes'] = array();
     $GLOBALS['nodes'][$node -> nid] = $node;
   }
 }
 
 /**
 * Implementation of hook_block()
 */
 function mapstraction_cck_block($op = 'list', $delta = 0, $edit = array()) {
  module_load_include('inc', 'mapstraction_cck', 'includes/mapstraction_cck.block');
 	return mapstraction_cck_block_implementation($op, $delta, $edit);
}

/**
 * Implementation of FAPI hook_elements().
 */
function mapstraction_cck_elements() {
  return array(
    'mapstraction_cck_picker' => array(
      '#input' => TRUE,
      '#columns' => array('lat', 'lon', 'wkt'),
      '#delta' => 0,
      '#process' => array('mapstraction_cck_picker_process'),
    ),
  );
}
function mapstraction_cck_picker_process($element, $edit, $form_state, $form) {
  
  $field = content_fields($element['#parents'][0]);

  $element['#title'] = $field['widget']['label'];
  //$element['#type'] = 'fieldset';

  //$element['map'] = array();  // reserve spot at top of form for map

  $element['wkt'] = array(
    '#type' => 'textarea',
    '#rows' => 2,
    '#title' => isset($element['#title']) ? $element['#title'] : NULL,
    '#default_value' => isset($element['#value']) ? $element['#value'] : NULL,
    '#required' => $field['required'],
    '#field_name' => $element['#field_name'],
    '#attributes' => array('rel' => MAPSTRACTION_CCK_MAP_ID_PREFIX .'-'. $field['field_name'])
  );

  geo_element_set_map($element);
  if (empty($element['#element_validate'])) {
    $element['#element_validate'] = array();
  }
  array_unshift($element['#element_validate'], 'mapstraction_cck_validate');

  // Make sure field info will be available to the validator which
  // does not get the values in $form.
  $form_state['#field_info'][$field['field_name']] = $field;
  return $element;
}

/**
 * FAPI validate function for custom element
 */
function mapstraction_cck_validate($element, &$form_state) {
  //Change the number level of the value to coordinate with the Geo architecture
  form_set_value($element, $element['#value']['wkt'], $form_state);

}
