<?php 
/**
 * Theme function for mapstraction_map
 */
function theme_mapstraction_map($map = array()) {
  $output = '
    <div id="'. $map['id'] .'" class="mapstraction_map" style="width:400px;height:300px;"></div>
  ';
  return $output;
}

/**
 * Returns all the providers included in IDELab Mapstraction
 * @return array 
 * 	Array with all providers
 */
function mapstraction_cck_providers(){
  return array(
    'google' => t('Google Maps'),
    'yahoo' => t('Yahoo! Maps'),
    'openlayers' => t('OpenLayers'),
    'cartociudad' => t('Cartociudad'),
    'microsoft' => t('Microsoft Virtual Earth'),
    'microsoft3D' => t('Microsoft Virtual Earth 3D')
  );
}

/**
 * Returns available providers in the application
 * @return array 
 * 	 Array with available providers in the application
 */
function mapstraction_cck_available_providers(){
  $providers = mapstraction_cck_providers();
  
  $selected_providers =  variable_get('mapstraction_cck_providers',array('openlayers' => t('OpenLayers'),
     																	'cartociudad' => t('Cartociudad'),
    																	'microsoft' => t('Microsoft Virtual Earth')));

  foreach($selected_providers as $provider =>$selected){
    
    if((string)$selected != "0"){

      $available_providers[$provider] = $providers[$provider];
    }
  }
  return $available_providers;
}
/**
 * Generates the map configuration form for hook_widget_settings()
 * @param $defaults 
 * 	Default values for the form
 * @return $form 
 * 	Configuration form
 */
function mapstraction_cck_map_form($defaults = array()){
// Map general properties
  
  // Form Properties
  $form['#tree'] = TRUE;
  $form['#cache'] = TRUE;
  
  $form['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#description' => t('Please use a CSS Width value.'),
    '#default_value' => $defaults['width'],
    '#maxlength' => 128,
  );
  
  $form['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#description' => t('Please use a CSS Height value.'),
    '#default_value' => $defaults['height'],
    '#maxlength' => 128,
  );
  
  $form['control'] = array(
    '#type' => 'radios',
    '#title' => t('Map controls'),
    '#description' => t('Select the kind of Controls you want to show'),
    '#options' => array('small'=>t('Small Controls'),'large'=>t('Large Controls')),
    '#default_value' => $defaults['control'],
  );
  $form['provider'] = array(
    '#type' => 'select',
    '#title' => t('Available providers'),
    '#description' => t('Select the Map provider you want to show'),
    '#options' => mapstraction_cck_available_providers(),
    '#default_value' => $defaults['provider'],
  );
  $form['features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Features settings'),
    '#description' => t('Administrator can customize some features properties'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['features']['width-msg']= array(
  	'#value' =>'<div id="mapstraction-cck-features-width-msg" class="messages error" style="display:none">'.
   	t('Stroke Width value must be a numeric value').'</div>',
  );
  $form['features']['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Stroke width'),
    '#description' => t('Set the Stroke with value in pixels. Blank value uses default provider width'),
    '#default_value' => $defaults['features']['width'],
    '#size' =>2,
    '#maxlength' => 2,
  );
  $form['features']['color-msg']= array(
  	'#value' =>'<div id="mapstraction-cck-features-color-msg" class="messages error" style="display:none">'.
   	t('Stroke color value must be a hex color value. I.E.: #FF00AB').'</div>',
  );
  $form['features']['color'] = array(
    '#type' => (module_exists('colorpicker') ? 'colorpicker_' : '') . 'textfield',
    '#title' => t('Stroke color'),
    '#description' => t('Set the Stroke color in hex values. For example:#000000. Blank value uses default provider color'),
    '#default_value' => $defaults['features']['color'],
    '#size' =>7,
    '#maxlength' => 7,
  );
  $form['features']['fillColor-msg']= array(
  	'#value' =>'<div id="mapstraction-cck-features-fillColor-msg" class="messages error" style="display:none">'.
   	t('Fill color value must be a hex color value. I.E.: #FF00AB').'</div>',
  );
  $form['features']['fillColor'] = array(
    '#type' => (module_exists('colorpicker') ? 'colorpicker_' : '') . 'textfield',
    '#title' => t('Fill color'),
    '#description' => t('Set the fill polygon color in hex values (Not supported by all providers). For example:#000000. Blank value uses default provider color'),
    '#default_value' => $defaults['features']['fillColor'],
    '#size' =>7,
    '#maxlength' => 7,
  );
  $form['features']['icon-msg']= array(
  	'#value' =>'<div id="mapstraction-cck-features-icon-msg" class="messages error" style="display:none">'.
   	t('Icon URL value must be an URL value. I.E.:http://www.myweb.com/icon.png').'</div>',
  );
  $form['features']['icon'] = array(
    '#type' => 'textfield',
    '#title' => t('Icon URL'),
    '#description' => t('Set the marker icon URL. Blank value uses default provider icon'),
    '#default_value' => $defaults['features']['icon'],
    
  );
  $form['bbox']['ne']['lat'] = array(
    '#type' =>'hidden',
    '#default_value' => $defaults['bbox']['ne']['lat'],
  );
  
  $form['bbox']['ne']['lon'] = array(
    '#type' =>'hidden',
    '#default_value' => $defaults['bbox']['ne']['lon'],
  );
  
  $form['bbox']['sw']['lat'] = array(
    '#type' =>'hidden',
    '#default_value' => $defaults['bbox']['sw']['lat'],
  );
  
  $form['bbox']['sw']['lon'] = array(
    '#type' =>'hidden',
    '#default_value' => $defaults['bbox']['sw']['lon'],
  );
  return $form;
}
/**
 * Method to load the specific scripts for different providers
 * @param $maps
 * 	string if you want to show one provider or array with various name provider
 */
function mapstraction_cck_load_maps($maps){
  //Load maps scripts and CSS 
  drupal_add_js(drupal_get_path('module', 'mapstraction_cck') .'/js/mapstraction.js');
  //drupal_add_js(drupal_get_path('module', 'mapstraction_cck') .'/js/mapstraction_cck.js');
    
  if(!is_array($maps)) $maps = array($maps => $maps);
  
  foreach ($maps as $key => $value) {
    $function = "mapstraction_cck_load_".$key."_provider";
    if(function_exists($function)){
      $result = call_user_func_array($function,array());  
    }
  }
  $path = check_plain(variable_get('mapstraction_cck_mapstraction_folder', 'http://mvn.idelab.uva.es/idelabmapstraction/downloads/1.0/idelabmapstraction.js'));
  
  // Check for full URL
  if (valid_url($path, TRUE)) {
    // If URL, we have to manually include it in Drupal
    $header = '<script src="'. check_url($path) .'" type="text/javascript"></script>';
    if(!isLoaded($header))drupal_set_html_head($header);
  }
  else {
    drupal_add_js($path.'/idelabmapstraction.js');
  }
  
}
/**
 * Google Maps script
 * @return array
 * 	Google Maps script and provider name
 */
function mapstraction_cck_load_google_provider(){
  $src = "http://maps.google.com/maps?file=api&amp;v=2&amp;key=".variable_get('mapstraction_cck_gm_key','');
  return mapstraction_cck_load_scripts($src);
}

/**
 * Yahoo! Maps script
 * @return array
 * 	Yahoo! Maps script and provider name
 */
function mapstraction_cck_load_yahoo_provider(){
  $src = "http://api.maps.yahoo.com/ajaxymap?v=3.8&appid=".variable_get('mapstraction_cck_ym_key','');
  return mapstraction_cck_load_scripts($src);
}

/**
 * Cartociudad script
 * @return array
 * 	Cartociudad script and provider name
 */
function mapstraction_cck_load_cartociudad_provider(){
  $src = 'http://openlayers.org/api/OpenLayers.js';
  return mapstraction_cck_load_scripts($src);
}

/**
 * OpenLayers script
 * @return array
 * 	OpenLayers script and provider name
 */
function mapstraction_cck_load_openlayers_provider(){
  $src = 'http://openlayers.org/api/OpenLayers.js';
  return mapstraction_cck_load_scripts($src);
}

/**
 * Microsoft script
 * @return array
 * 	Microsoft script and provider name
 */
function mapstraction_cck_load_microsoft_provider(){
  $src = 'http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=6.2';
  return mapstraction_cck_load_scripts($src);
}

/**
 * Microsoft3D script
 * @return array
 * Microsoft3D script and provider name
 */
function mapstraction_cck_load_microsoft3D_provider(){
  $src = 'http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=6.2';
  return mapstraction_cck_load_scripts($src);
}

/**
 * Loads the neccesary scripts to show the maps
 * @param $src
 * 	The specific provider script URL
 * @param $provider
 * 	The provider name
 */
function mapstraction_cck_load_scripts($src){
  $header = "<script type='text/javascript' src='$src'></script>\n";
  if(!isLoaded($header))drupal_set_html_head($header);
  
}

/**
 * Checks if a scripts has been already loaded or not
 * @param $header 
 * 	The script header
 * @return 
 * 	Boolean TRUE if it is loaded or FALSE if not loaded
 */
function isLoaded($header){
  if(strpos(drupal_get_html_head(),$header))return TRUE;
  else return FALSE;
}



/**
 * Render Map
 *
 * Given perimeters, render an Mapstraction map
 *
 * @param $map
 *   Associative array of map paramters
 * @param $render
 *   Boolean whether to fully render (include theme and JS)
 *  
 * @return
 *   Boolean if successful
 */
function mapstraction_cck_render_map($map = array(), $render = TRUE) {
  // Check array
  if (!is_array($map)) {
    return FALSE;
  }
  if(!isset($map['height'])) $map['height'] = '400px';
  if(!isset($map['width'])) $map['width'] = '100%';
  if(!isset($map['control'])) $map['control'] = 'small';
  if(!isset($map['provider'])){$map['provider'] = variable_get('mapstraction_cck_default_provider','openlayers');}
  
    // Add map container to drupal JS settings
    $mapstraction = array(
      'mapstraction' => array(
        'maps' => array(
          $map['id'] => $map,
        ),
      ),
    );
    drupal_add_js($mapstraction, 'setting');
    
    // Add themed HTML (no need for it to go to JS)
    $map['themed'] = theme('mapstraction_map', $map);
  
  // Return map with or without errors
  return $map;
}
/**
 * Validates the config map form
 * @param $widget
 * 	Form values to validate
 */
function mapstraction_cck_map_form_validate($widget){
  $pattern = "/^[0-9]+(px|%)$/";
  $values = array('height' => t('Height'), 'width' => t('Width'));
  foreach ($values as $key => $value) {
    if(preg_match($pattern,$widget[$key])){
      if($widget[$key] != ereg_replace("%","",$widget[$key])){
        if((int)ereg_replace("%","",$widget[$key]) > 100)
          form_set_error('mapstraction_cck['.$key, t('The @field value isn\'t correct. It mustn\'t bigger than 100%',array('@field'=>$value)));
      }
    }else{
      form_set_error('mapstraction_cck['.$key, t('The @field value isn\'t correct. It must be a correct CSS value',array('@field'=>$value)));
    }
  }
  $pattern = "/^#([0-9]|[a-f]){6}$/i";
  $values = array('color' => t('Stroke Color'), 'fillColor' => t('Fill Color'));
  foreach ($values as $key => $value) { 
    if($widget['features'][$key]!= "" && !preg_match($pattern,$widget['features'][$key]))
      form_set_error('mapstraction_cck[features]['.$key, t('The @field value isn\'t correct. It must be a correct Hex color value. For example #00FFAA',array('@field'=>$value)));
  }
    if($widget['features']['icon']!= "" && !valid_url($widget['features']['icon'],TRUE))
      form_set_error('mapstraction_cck[features][icon', t('The Icon URL value isn\'t corret. It must be a correct URL path. For example http://www.itastdevserver.tel.uva.es/icon.png'));
  
    if($widget['features']['width']!= "" && !is_numeric($widget['features']['width']))
      form_set_error('mapstraction_cck[features][width', t('The Stroke Width value isn\'t correct. It must be a numeric value'));
}

/**
 *  Extract the features properties from a field
 * @param $field
 * 	filed to extract features properties for
 * @return array
 * 	Contains the specific feature fiel dproperties 
 */
function mapstraction_cck_feature_data($field){
  if($field['geo_type'] =='point'){
   if(!empty($field['widget']['mapstraction_cck']['features']['icon']))$data['feature']['icon'] = $field['widget']['mapstraction_cck']['features']['icon'];
  }else{
    if(!empty($field['widget']['mapstraction_cck']['features']['width']))$data['feature']['width'] = intval($field['widget']['mapstraction_cck']['features']['width']);
    if(!empty($field['widget']['mapstraction_cck']['features']['color']))$data['feature']['color'] = $field['widget']['mapstraction_cck']['features']['color'];
    if($field['geo_type'] == 'polygon')
      if(!empty($field['widget']['mapstraction_cck']['features']['fillColor'] ))$data['feature']['fillColor'] = $field['widget']['mapstraction_cck']['features']['fillColor'];
  }
  return $data;
}
/**
 * Return the instructions for themap widget. These are different for each provider
 * @param $provider
 * @return string
 * 	Specific provider widget instructions
 */
function mapstraction_cck_use_map($provider){
  $desc = t('Click the controls above the map to switch between edition mode and zoom/pan mode or add a new feature to the map.'); 
  if($provider == 'openlayers' || $provider == 'cartociudad' || $provider == 'google')
    $desc.= t('Draw your feature, double-clicking to finish. You may edit your shape using the control points.');
  else
    $desc.= t('Draw your feature, click auxiliar marker to finish. You may edit your shape using the auxiliar markers. To deselect a shape, press the Esc Key. ');
  $desc.= t('To delete a shape, select it and press the delete key.');
  return $desc;
}

/**
 * Returns the default Bounding Box for Maps
 * @return array
 * 	Default Bounding Box for maps
 */
function mapstraction_cck_default_bounds(){
  return array(
   'ne' => array(
     'lat' =>'46.61926103617151',
     'lon' =>'15.732421875',  
   ),
   'sw' => array(
  	 'lat' =>'33.21111647241685',
     'lon' =>'-24.3017578125',
   )
  );
}
