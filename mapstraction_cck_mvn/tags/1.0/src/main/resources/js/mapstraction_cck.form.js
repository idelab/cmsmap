/**
 * Global Object for Namespace
 */
var Mapstraction = Mapstraction || {};
Mapstraction.CCK = Mapstraction.CCK || {};
Mapstraction.CCK.config = Mapstraction.CCK.config || {};

/**
 * Onload page event which starts the map proccessing
 */
$(document).ready(function(){
  if(Mapstraction.isSet(Drupal.settings.mapstraction_cck)){
	
    Mapstraction.CCK.maps = Drupal.settings.mapstraction_cck.maps;
	
  
    // Go through CCK Fields and display map
    var fieldContainer = '';
    for (var mapid in Mapstraction.CCK.maps) {
  	  Drupal.settings.mapstraction.maps[mapid].handlers = {};
  	  Drupal.settings.mapstraction.maps[mapid].handlers.load = Mapstraction.CCK.config.init;
  	  fieldContainer = Mapstraction.CCK.maps[mapid].field_container;
      // Add Themed Map container
      $('#' + fieldContainer).before(Mapstraction.CCK.maps[mapid].field_map_themed);
    }
  }
  Drupal.behaviors.mapstraction();
	
});

/**
 * Initializes the widget map. Add Events to the map and add initial Features
 * @param event
 * 	The event name
 * @param map
 * 	The map to initialize
 */
Mapstraction.CCK.config.init = function(event,map){
	if(Mapstraction.isSet(Mapstraction.CCK.config.features)) return;
  	var neLat = $('#edit-mapstraction-cck-bbox-ne-lat').val();
  	var neLon = $('#edit-mapstraction-cck-bbox-ne-lon').val();
  	var swLat = $('#edit-mapstraction-cck-bbox-sw-lat').val();
  	var swLon = $('#edit-mapstraction-cck-bbox-sw-lon').val();
  	var bbox = new mxn.BoundingBox(swLat,swLon,neLat,neLon);
  	Mapstraction.centerMap(Mapstraction.maps[map.mapid].map,bbox);  
  	//Attach config events
  	$('#edit-mapstraction-cck-provider').change(function(){
		var provider =$(this).val();

		var data ={}

		$("input[name^='mapstraction_cck[features]']").each(function(){
		 	data[this.id.split('-')[this.id.split('-').length -1]] = $(this).val()
		})
		data = Mapstraction.CCK.config.validateData(data); 
		Mapstraction.CCK.config.deleteFeatures(map);
		Mapstraction.maps[map.mapid].map.endPan.removeHandler(Mapstraction.CCK.config.mapMoved);
		Mapstraction.CCK.config.swapProvider(map,'config-'+provider,provider);
		Mapstraction.CCK.config.createFeatures(map,data);
		Mapstraction.maps[map.mapid].map.endPan.addHandler(Mapstraction.CCK.config.mapMoved);
  	})
  	
  	Mapstraction.CCK.config.createFeatures(map);

	var data ={}
	$("input[name^='mapstraction_cck[features]']").each(function(){
	 	data[this.id.split('-')[this.id.split('-').length -1]] = $(this).val()
	})
	Mapstraction.CCK.config.alterFeatures(data);
  	$("input[name^='mapstraction_cck[features]']").blur(function(){
  		var data = {};
  		var config = $(this).attr('id').split('-')[$(this).attr('id').split('-').length - 1];
  		data[config] = $(this).val()
  		Mapstraction.CCK.config.alterFeatures(data);
  	})
  	
  	Mapstraction.maps[map.mapid].map.endPan.addHandler(Mapstraction.CCK.config.mapMoved);
  	
  	/*$( "input[name='mapstraction_cck[control]']" ).click(function(){
  		map.addControls({});
  		if(this.value == 'small') map.addSmallControls()
  		else map.addLargeControls();		 
	});*/
}

/**
 * Adds the auxiliar features to the config map
 * @param map
 * 	The map to add features
 * @param data
 * 	The specific features settings
 */
Mapstraction.CCK.config.createFeatures = function(map,data){
  	
  	var points = []
  	Mapstraction.CCK.config.features = [];
  	var center = map.getCenter();
  	var bbox=map.getBounds();
  	var dst = Math.abs((bbox.getNorthEast().lon - bbox.getSouthWest().lon)/10);
  	Mapstraction.CCK.config.features.marker = new mxn.Marker(center); 
  	if(data) Mapstraction.CCK.config.features.marker.addData(data)
  	map.addMarker(Mapstraction.CCK.config.features.marker);
	
  	points.push(new mxn.LatLonPoint(center.lat - 0.5*dst, center.lon + 0.5*dst));
  	points.push(new mxn.LatLonPoint(center.lat + 0.5*dst, center.lon + 1*dst));
	points.push(new mxn.LatLonPoint(center.lat - 0.5*dst, center.lon + 1.5*dst));
	points.push(new mxn.LatLonPoint(center.lat + 0.5*dst, center.lon + 2*dst));
	Mapstraction.CCK.config.features.line = new mxn.Polyline(points);
	if(data) Mapstraction.CCK.config.features.line.addData(data);
	map.addPolyline(Mapstraction.CCK.config.features.line);
	
	points=[];
	points.push(new mxn.LatLonPoint(center.lat - 0.5*dst, center.lon - 0.5*dst));
	points.push(new mxn.LatLonPoint(center.lat + 0.5*dst, center.lon - 0.5*dst));
	points.push(new mxn.LatLonPoint(center.lat + 0.5*dst, center.lon - 1.5*dst));
	points.push(new mxn.LatLonPoint(center.lat - 0.5*dst, center.lon - 1.5*dst));
	Mapstraction.CCK.config.features.poly = new mxn.Polyline(points);
	Mapstraction.CCK.config.features.poly.setClosed(true);
	if(data) Mapstraction.CCK.config.features.poly.addData(data)
	map.addPolyline(Mapstraction.CCK.config.features.poly);
}

/**
 * Delete the auxiliar config map features
 * @param map
 * 	The map to remove features 
 */
Mapstraction.CCK.config.deleteFeatures = function(map){
	if(Mapstraction.isSet(Mapstraction.CCK.config.features))
	map.removeMarker(Mapstraction.CCK.config.features.marker);
	Mapstraction.CCK.config.features.marker = null;
	map.removePolyline(Mapstraction.CCK.config.features.line);
	Mapstraction.CCK.config.features.line = null;
	map.removePolyline(Mapstraction.CCK.config.features.poly);
	Mapstraction.CCK.config.features.poly = null;
}

/**
 * Modify the auxiliar Features settings
 * @param data
 * 	The NEW specific features settings
 * 	 
 */
Mapstraction.CCK.config.alterFeatures = function(data){
	data = Mapstraction.CCK.config.validateData(data);
	if(data == {}) return;
	for (var type in Mapstraction.CCK.config.features){
		Mapstraction.CCK.config.features[type].addData(data);
		Mapstraction.CCK.config.features[type].updateProprietary();
	}
}

/**
 * Real time validation of feature settings form
 * @param data
 * 	The new specific feature settings to validate
 */
Mapstraction.CCK.config.validateData = function(data){
	var aux = {};
	if(data.width == ''){
		aux.width = null;
	}else{
		if (data.width ){
			if(parseInt(data.width) > 0){
				aux.width = data.width;
			}
			else{
				$('#mapstraction-cck-features-width-msg').show("slow");
		      	setTimeout("$('#mapstraction-cck-features-width-msg').hide('fast');",3000);
		    }
		}
	}
	

	if(data.color == '' || data.color == "#"){
		aux.color = null;
	}
	else{
		if(data.color && data.color != ""){
			var pattern = /^#([0-9]|[a-f]){6}$/i;
			if(data.color.match(pattern)){
				aux.color = data.color;
			}
			else{
				$('#mapstraction-cck-features-color-msg').show("slow");
		      	setTimeout("$('#mapstraction-cck-features-color-msg').hide('fast');",3000);
		    }
		}
	}
	
	if(data.fillColor == '' || data.fillColor == "#"){
		aux.fillColor = null;
	}
	else{
		if(data.fillColor && data.fillColor != "" ){
			var pattern = /^#([0-9]|[a-f]){6}$/i;
			if(data.fillColor.match(pattern)){
				aux.fillColor = data.fillColor;
			}
			else{
				$('#mapstraction-cck-features-fillColor-msg').show("slow");
		      	setTimeout("$('#mapstraction-cck-features-fillColor-msg').hide('fast');",3000);
		    }
		}
	}
	
	if(data.icon == ''){
		aux.icon = null;
	}
	else{
		if(data.icon && data.icon != ""){	
			var pattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
			if (data.icon.match(pattern)){
				aux.icon = data.icon;	
			}else{
				$('#mapstraction-cck-features-icon-msg').show("slow");
		      	setTimeout("$('#mapstraction-cck-features-icon-msg').hide('fast');",3000);
		    }
		}
	}
	return aux;
}

/**
 * Method that changes the map provider
 * @param mapid
 * 	ID of the map to change
 * @param element
 * 	The div element where the map will be showed
 * @param provider
 * 	The new provider for the map 
 */
 
Mapstraction.CCK.config.swapProvider = function(map,element,provider){
	map.swap(element,provider);
}

/**
 * Method to capture the endPan map event.
 * Saves the new Bounding box to default Bounding Box
 * Moves the auxiliar features to the new map Center
 * 
 * @param event
 * 	Event Name
 * @param map
 * 	The Map that fires the event
 */
Mapstraction.CCK.config.mapMoved = function(event,map){
	var bbox = map.getBounds();
	$('#edit-mapstraction-cck-bbox-ne-lat').val(bbox.ne.lat);
	$('#edit-mapstraction-cck-bbox-ne-lon').val(bbox.ne.lon);
	$('#edit-mapstraction-cck-bbox-sw-lat').val(bbox.sw.lat);
	$('#edit-mapstraction-cck-bbox-sw-lon').val(bbox.sw.lon);
	var data ={}
	$("input[name^='mapstraction_cck[features]']").each(function(){
	 	data[this.id.split('-')[this.id.split('-').length -1]] = $(this).val()
	})
	Mapstraction.CCK.config.deleteFeatures(map);
	Mapstraction.CCK.config.createFeatures(map,data);
}

