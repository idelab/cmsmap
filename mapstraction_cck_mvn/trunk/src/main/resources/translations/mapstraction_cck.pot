# $Id$
#
# LANGUAGE translation of Drupal (general)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  mapstraction_cck.inc: n/a
#  modules/mapstraction_views/mapstraction_views_style_map.inc: n/a
#  mapstraction_cck.module: n/a
#  mapstraction_cck.info: n/a
#  modules/mapstraction_cck_inline/mapstraction_cck_inline.info: n/a
#  modules/mapstraction_views/mapstraction_views.info: n/a
#  js/mapstraction_cck.js: n/a
#  modules/mapstraction_cck_inline/mapstraction_cck_inline.module: n/a
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2010-05-24 10:15+0200\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: mapstraction_cck.inc:20;36
msgid "Google Maps"
msgstr ""

#: mapstraction_cck.inc:21
msgid "Yahoo! Maps"
msgstr ""

#: mapstraction_cck.inc:22;49
msgid "OpenLayers"
msgstr ""

#: mapstraction_cck.inc:23;50
msgid "Cartociudad"
msgstr ""

#: mapstraction_cck.inc:24;51
msgid "Microsoft Virtual Earth"
msgstr ""

#: mapstraction_cck.inc:25
msgid "Microsoft Virtual Earth 3D"
msgstr ""

#: mapstraction_cck.inc:37
msgid "Cartociudad (Only for Spain)"
msgstr ""

#: mapstraction_cck.inc:99;389
msgid "Width"
msgstr ""

#: mapstraction_cck.inc:100
msgid "Please use a CSS Width value."
msgstr ""

#: mapstraction_cck.inc:107;389
msgid "Height"
msgstr ""

#: mapstraction_cck.inc:108
msgid "Please use a CSS Height value."
msgstr ""

#: mapstraction_cck.inc:117
msgid "You need the 1.1 version of IDELabMapstraction library if you want to use the geocoding service"
msgstr ""

#: mapstraction_cck.inc:121
msgid "Geocode provider"
msgstr ""

#: mapstraction_cck.inc:122 modules/mapstraction_views/mapstraction_views_style_map.inc:97
msgid "None"
msgstr ""

#: mapstraction_cck.inc:123
msgid "Select whether a field for geocode info to center the map in a determinate location is displayed"
msgstr ""

#: mapstraction_cck.inc:130
msgid "Map controls"
msgstr ""

#: mapstraction_cck.inc:131
msgid "Select the kind of Controls you want to show"
msgstr ""

#: mapstraction_cck.inc:132
msgid "Small Controls"
msgstr ""

#: mapstraction_cck.inc:132
msgid "Large Controls"
msgstr ""

#: mapstraction_cck.inc:137 mapstraction_cck.module:700
msgid "Available providers"
msgstr ""

#: mapstraction_cck.inc:138
msgid "Select the Map provider you want to show"
msgstr ""

#: mapstraction_cck.inc:144
msgid "Features settings"
msgstr ""

#: mapstraction_cck.inc:145
msgid "Administrator can customize some features properties"
msgstr ""

#: mapstraction_cck.inc:152
msgid "Stroke Width value must be a numeric value"
msgstr ""

#: mapstraction_cck.inc:156
msgid "Stroke width"
msgstr ""

#: mapstraction_cck.inc:157
msgid "Set the Stroke with value in pixels. Blank value uses default provider width"
msgstr ""

#: mapstraction_cck.inc:164
msgid "Stroke color value must be a hex color value. I.E.: #FF00AB"
msgstr ""

#: mapstraction_cck.inc:168
msgid "Stroke color"
msgstr ""

#: mapstraction_cck.inc:169
msgid "Set the Stroke color in hex values. For example:#000000. Blank value uses default provider color"
msgstr ""

#: mapstraction_cck.inc:176
msgid "Fill color value must be a hex color value. I.E.: #FF00AB"
msgstr ""

#: mapstraction_cck.inc:180
msgid "Fill color"
msgstr ""

#: mapstraction_cck.inc:181
msgid "Set the fill polygon color in hex values (Not supported by all providers). For example:#000000. Blank value uses default provider color"
msgstr ""

#: mapstraction_cck.inc:188
msgid "Icon URL value must be an URL value. I.E.:http://www.myweb.com/icon.png"
msgstr ""

#: mapstraction_cck.inc:192
msgid "Icon URL"
msgstr ""

#: mapstraction_cck.inc:193
msgid "Set the marker icon URL. Blank value uses default provider icon"
msgstr ""

#: mapstraction_cck.inc:394
msgid "The @field value isn't correct. It mustn't bigger than 100%"
msgstr ""

#: mapstraction_cck.inc:397
msgid "The @field value isn't correct. It must be a correct CSS value"
msgstr ""

#: mapstraction_cck.inc:401
msgid "Stroke Color"
msgstr ""

#: mapstraction_cck.inc:401
msgid "Fill Color"
msgstr ""

#: mapstraction_cck.inc:404
msgid "The @field value isn't correct. It must be a correct Hex color value. For example #00FFAA"
msgstr ""

#: mapstraction_cck.inc:407
msgid "The Icon URL value isn't corret. It must be a correct URL path. For example http://www.itastdevserver.tel.uva.es/icon.png"
msgstr ""

#: mapstraction_cck.inc:410
msgid "The Stroke Width value isn't correct. It must be a numeric value"
msgstr ""

#: mapstraction_cck.inc:438
msgid "Click the controls above the map to switch between edition mode and zoom/pan mode or add a new feature to the map."
msgstr ""

#: mapstraction_cck.inc:440
msgid "Draw your feature, double-clicking to finish. You may edit your shape using the control points."
msgstr ""

#: mapstraction_cck.inc:442
msgid "Draw your feature, click auxiliar marker to finish. You may edit your shape using the auxiliar markers. To deselect a shape, press the Esc Key. "
msgstr ""

#: mapstraction_cck.inc:443
msgid "To delete a shape, select it and press the delete key."
msgstr ""

#: mapstraction_cck.module:28
msgid "The Mapstraction CCK module provides widgets and field formatters that interface with Mapstraction."
msgstr ""

#: mapstraction_cck.module:101
msgid "The source for the IDELab Mapstraction library can be one of two things:"
msgstr ""

#: mapstraction_cck.module:103
msgid "URL: This means that the IDELab Mapstraction JS library is not hosted on this site. IDELab Mapstraction provides a hosted JS file.  By default the Mapstraction CCK module will use this, since the JS file cannot be included with the module.  This is @ol_api_url.  This may effect performance as it is not hosted on your site. "
msgstr ""

#: mapstraction_cck.module:105
msgid "Drupal Path: This is a path relative to the Drupal base.  For instance, if you <a href=\"!ol_url\">Download IDELABMapstraction</a> manually to the Mapstraction CCK module folder and extract it, you may use a value like: @suggested_path"
msgstr ""

#: mapstraction_cck.module:111
msgid "The source for the OpenLayers library can be one of two things:"
msgstr ""

#: mapstraction_cck.module:113
msgid "URL: This means that the OpenLayers JS library is not hosted on this site. OpenLayers provides a hosted JS file. By default the Open Layers module will use this, since the JS file cannot be included with the module.  This is @ol_api_url. This may effect performance as it is not hosted on your site. Also, depending on how the OpenLayers JS API changes, this module may not be compatible with this version."
msgstr ""

#: mapstraction_cck.module:115
msgid "Drupal Path: This is a path relative to the Drupal base. For instance, if you <a href=\"!ol_url\">Download OpenLayers</a> manually to the OpenLayers module folder and extract it, you may use a value like: @suggested_path"
msgstr ""

#: mapstraction_cck.module:124
msgid "IDELab Mapstraction Library folder"
msgstr ""

#: mapstraction_cck.module:134
msgid "OpenLayers Library folder"
msgstr ""

#: mapstraction_cck.module:144
msgid "Google Maps API Key"
msgstr ""

#: mapstraction_cck.module:148
msgid "Your personal Google Maps API key.  You must get this for each separate website at <a href='http://www.google.com/apis/maps/'>Google Map API website</a>."
msgstr ""

#: mapstraction_cck.module:153
msgid "Yahoo! Maps API Key"
msgstr ""

#: mapstraction_cck.module:157
msgid "Your personal Yahoo! Maps API key.  You must get this for each separate website at <a href='http://developer.yahoo.com/maps/'>Yahoo! Maps API website</a>."
msgstr ""

#: mapstraction_cck.module:166
msgid "Providers"
msgstr ""

#: mapstraction_cck.module:167
msgid "Choose the providers that can be used in the application."
msgstr ""

#: mapstraction_cck.module:175
msgid "Default provider"
msgstr ""

#: mapstraction_cck.module:176
msgid "Choose the default provider that will be used in the application."
msgstr ""

#: mapstraction_cck.module:196
msgid "Google Maps API key is required for Google Maps provider."
msgstr ""

#: mapstraction_cck.module:199
msgid "Yahoo! Maps API key is required for Yahoo! Maps provider."
msgstr ""

#: mapstraction_cck.module:213
msgid "You can set as default a non selected provider"
msgstr ""

#: mapstraction_cck.module:226
msgid "Mapstraction Map"
msgstr ""

#: mapstraction_cck.module:270
msgid "Map Settings"
msgstr ""

#: mapstraction_cck.module:271
msgid "These are the settings that determine how the map will look."
msgstr ""

#: mapstraction_cck.module:319
msgid "Geocode field"
msgstr ""

#: mapstraction_cck.module:321
msgid "Center Map"
msgstr ""

#: mapstraction_cck.module:323
msgid "Insert an address to center the map"
msgstr ""

#: mapstraction_cck.module:331
msgid "You can't add more features to the map"
msgstr ""

#: mapstraction_cck.module:334
msgid "Add new Feature"
msgstr ""

#: mapstraction_cck.module:335
msgid "Activate Editing"
msgstr ""

#: mapstraction_cck.module:336
msgid "Deactivate Editing"
msgstr ""

#: mapstraction_cck.module:345
msgid "Show/Hide WKT Fields"
msgstr ""

#: mapstraction_cck.module:362
msgid "Configure the map:"
msgstr ""

#: mapstraction_cck.module:368
msgid "Pan the map and select your own Bounding Box. This will be the widget default Bounding Box. In the map features you can see how it will look in the map in real time."
msgstr ""

#: mapstraction_cck.module:381
msgid "Mapstraction CCK Grouped Map"
msgstr ""

#: mapstraction_cck.module:386
msgid "Mapstraction CCK Single Map"
msgstr ""

#: mapstraction_cck.module:653
msgid "Mapstraction CCK Grouped Map display uses first field selected provider"
msgstr ""

#: mapstraction_cck.module:675
msgid "Shows the page geographic info"
msgstr ""

#: mapstraction_cck.module:682
msgid "Shows the nearest nodes"
msgstr ""

#: mapstraction_cck.module:695;730
msgid "Title"
msgstr ""

#: mapstraction_cck.module:701
msgid "Select the Map provider you want to show in the block"
msgstr ""

#: mapstraction_cck.module:707
msgid "Show controls"
msgstr ""

#: mapstraction_cck.module:708
msgid "Select whether controls are displayed on the map or not"
msgstr ""

#: mapstraction_cck.module:714
msgid "Show information bubbles"
msgstr ""

#: mapstraction_cck.module:715
msgid "Select whether bubbles with link to node are displayed on the map markers or not"
msgstr ""

#: mapstraction_cck.module:721
msgid "Show markers associated to polylines' centroid"
msgstr ""

#: mapstraction_cck.module:722
msgid "Select whether a marker placed on the polylines's centroid is displayed or not"
msgstr ""

#: mapstraction_cck.module:735
msgid "Geographical units"
msgstr ""

#: mapstraction_cck.module:736
msgid "Select the units you want to use in the block"
msgstr ""

#: mapstraction_cck.module:742
msgid "Distance"
msgstr ""

#: mapstraction_cck.module:743
msgid "Select the max distance you want to filter."
msgstr ""

#: mapstraction_cck.module:750
msgid "Max number of nodes"
msgstr ""

#: mapstraction_cck.module:753
msgid "Select the max number of nodes you want to show. 0 or blank value for unlimited."
msgstr ""

#: mapstraction_cck.module:975
msgid "node"
msgstr ""

#: mapstraction_cck.module:975
msgid "distance"
msgstr ""

#: mapstraction_cck.module:83
msgid "Mapstraction CCK module settings"
msgstr ""

#: mapstraction_cck.module:84
msgid "Select map providers you want to offer"
msgstr ""

#: mapstraction_cck.info:0
msgid "Mapstraction CCK"
msgstr ""

#: mapstraction_cck.info:0
msgid "Provides CCK fields and widgets through an Mapstraction interface."
msgstr ""

#: mapstraction_cck.info:0 modules/mapstraction_cck_inline/mapstraction_cck_inline.info:0 modules/mapstraction_views/mapstraction_views.info:0
msgid "Geo"
msgstr ""

#: js/mapstraction_cck.js:0
msgid "WKT is not valid"
msgstr ""

#: modules/mapstraction_cck_inline/mapstraction_cck_inline.module:20
msgid "The Mapstraction CCK Inline module provides input filters to allow for inline Mapstraction maps."
msgstr ""

#: modules/mapstraction_cck_inline/mapstraction_cck_inline.info:0
msgid "Mapstraction CCK Inline"
msgstr ""

#: modules/mapstraction_cck_inline/mapstraction_cck_inline.info:0
msgid "Provides the way to show a Mapstraction Map in an inline map."
msgstr ""

#: modules/mapstraction_views/mapstraction_views_style_map.inc:27
msgid "Mapping API"
msgstr ""

#: modules/mapstraction_views/mapstraction_views_style_map.inc:28
msgid "Select the Mapstraction API to use for this view."
msgstr ""

#: modules/mapstraction_views/mapstraction_views_style_map.inc:36
msgid "Dimensions"
msgstr ""

#: modules/mapstraction_views/mapstraction_views_style_map.inc:45
msgid "Initial point"
msgstr ""

#: modules/mapstraction_views/mapstraction_views_style_map.inc:50
msgid "Auto detect initial point"
msgstr ""

#: modules/mapstraction_views/mapstraction_views_style_map.inc:51
msgid "If checked, the map will auto detect initial coordinates and zoom level based on all markers on the map. Otherwise you can specify these values by yourself."
msgstr ""

#: modules/mapstraction_views/mapstraction_views_style_map.inc:57
msgid "Latitude"
msgstr ""

#: modules/mapstraction_views/mapstraction_views_style_map.inc:69
msgid "Longitude"
msgstr ""

#: modules/mapstraction_views/mapstraction_views_style_map.inc:81
msgid "Zoom level"
msgstr ""

#: modules/mapstraction_views/mapstraction_views_style_map.inc:93
msgid "Zoom Control"
msgstr ""

#: modules/mapstraction_views/mapstraction_views_style_map.inc:95
msgid "Large"
msgstr ""

#: modules/mapstraction_views/mapstraction_views_style_map.inc:96
msgid "Small"
msgstr ""

#: modules/mapstraction_views/mapstraction_views_style_map.inc:105
msgid "You need at least one field before you can configure your field settings"
msgstr ""

#: modules/mapstraction_views/mapstraction_views_style_map.inc:127
msgid "Select the fields that will be showed in the map. In the Bubble field you can choose the field that will be displayed when someone clicks over a marker. Then, select the geospatial fields that will be displayed on the map."
msgstr ""

#: modules/mapstraction_views/mapstraction_views_style_map.inc:132
msgid "Geographic fields"
msgstr ""

#: modules/mapstraction_views/mapstraction_views_style_map.inc:133
msgid "Choose the fields that will be showed in the map. <b>You must select WKT fields</b>"
msgstr ""

#: modules/mapstraction_views/mapstraction_views_style_map.inc:152
msgid "The display style for @field must be set to decimal degrees."
msgstr ""

#: modules/mapstraction_views/mapstraction_views.info:0
msgid "Mapstraction Views"
msgstr ""

#: modules/mapstraction_views/mapstraction_views.info:0
msgid "Mapstraction Views allows you to display maps  in views using different mapping APIs."
msgstr ""

