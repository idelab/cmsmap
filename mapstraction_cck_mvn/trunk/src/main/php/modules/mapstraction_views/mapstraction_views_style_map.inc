<?php
// $Id$

/**
 * @file
 * Defines a style plugin that will display points on a map from a number of
 * different providers.
 */
class mapstraction_views_style_map extends views_plugin_style {
  function option_definition() {
    $options['dimensions'] = array(
      'default' => '600x400'
    );
    $options['initial_point'] = array(
      'auto' => TRUE,
      'latitude' => '',
      'longitude' => '',
      'zoom' => 10,
    );

    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    $form['api'] = array(
      '#type' => 'select',
      '#title' => t('Mapping API'),
      '#description' => t('Select the Mapstraction API to use for this view.'),
      '#options' => mapstraction_cck_available_providers(),
      '#default_value' => $this->options['api'],
    );
    
    
    $form['dimensions'] = array(
      '#type' => 'textfield',
      '#title' => t('Dimensions'),
      '#size' => 10,
      '#maxlength' => 255,
      '#default_value' => $this->options['dimensions'],
    );
    
    $form['initial_point'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => t('Initial point'),
    );

    $form['initial_point']['auto'] = array(
      '#type' => 'checkbox',
      '#title' => t('Auto detect initial point'),
      '#description' => t('If checked, the map will auto detect initial coordinates and zoom level based on all markers on the map. Otherwise you can specify these values by yourself.'),
      '#default_value' => isset($this->options['initial_point']['auto'])?$this->options['initial_point']['auto']: TRUE,
    );

    $form['initial_point']['latitude'] = array(
      '#type' => 'textfield',
      '#title' => t('Latitude'),
      '#size' => 40,
      '#maxlength' => 255,
      '#process' => array('views_process_dependency'),
      '#dependency' => array(
        'edit-style-options-initial-point-auto' => array(0)
      ),
      '#default_value' => $this->options['initial_point']['latitude'],
    );

    $form['initial_point']['longitude'] = array(
      '#type' => 'textfield',
      '#title' => t('Longitude'),
      '#size' => 40,
      '#maxlength' => 255,
      '#process' => array('views_process_dependency'),
      '#dependency' => array(
        'edit-style-options-initial-point-auto' => array(0)
      ),
      '#default_value' => $this->options['initial_point']['longitude'],
    );

    $form['initial_point']['zoom'] = array(
      '#type' => 'textfield',
      '#title' => t('Zoom level'),
      '#size' => 2,
      '#maxlength' => 2,
      '#process' => array('views_process_dependency'),
      '#dependency' => array(
        'edit-style-options-initial-point-auto' => array(0)
      ),
      '#default_value' => $this->options['initial_point']['zoom'],
    );
    
    $form['zoom_control'] = array(
      '#type' => 'select',
      '#title' => t('Zoom Control'),
      '#options' => array(
        'large' => t('Large'),
        'small' => t('Small'),
        'none' => t('None')
      ),
      '#default_value' => $this->options['zoom_control'],
    );
    
    $handlers = $this->display->handler->get_handlers('field');
    if (empty($handlers)) {
      $form['error_markup'] = array(
        '#value' => t('You need at least one field before you can configure your field settings'),
        '#prefix' => '<div class="error form-item description">',
        '#suffix' => '</div>',
      );
    }
    else {
      $field_names[$field] = array('' => '--');
      foreach ($handlers as $field => $handler) {
        if ($label = $handler->label()) {
          $field_names[$field] = $label;
          if(isset($handler->geo))$geo_fields[$field] = $label;
        }
        else {
          $field_names[$field] = $handler->ui_name();
          if(isset($handler->geo))$geo_fields[$field] = $handler->ui_name();
        }
        
      }

      $form['fields'] = array(
        '#type' => 'fieldset',
        '#title' => 'Field usage',
        '#description' => t('Select the fields that will be showed in the map. In the Bubble field you can choose the field that will be displayed when someone clicks over a marker. Then, select the geospatial fields that will be displayed on the map.'),
      );

      $form['fields']['wkt'] = array(
      	'#type' => 'checkboxes',
        '#title' => t('Geographic fields'),
        '#description' => t('Choose the fields that will be showed in the map. <b>You must select WKT fields</b>'),
        '#options' => $geo_fields,
        '#default_value' => isset($this->options['fields']['wkt'])?$this->options['fields']['wkt']:array(),
        '#required' => TRUE,
      );
    }
  }
    
  function validate() {
    $errors = array();
    
    // Validate the field style for location.module's lat/lon fields. They
    // must be set to decimal degress.
    $fields = $this->display->handler->get_handlers('field');
    $field_array = array($fields[$this->options['fields']['latitude']],
    $fields[$this->options['fields']['longitude']]);
    
    foreach ($field_array as $field) {
      if ($field->options['table'] == 'location' && $field->options['style'] != 'dd') {
        $errors[] = t('The display style for @field must be set to decimal degrees.', array('@field' => $field->options['label']));
      }
    }
    
    return $errors;
  }
  
  function render() {
    if ($this->view->preview == TRUE) {
      return '<div class="messages error">Preview is disabled for the Mapstraction style plugin.</div>';
    }
    else {
      // Would like to use parent::render() here but there seems to be problem
      // with the way the theme function is handled. Copying this from
      // views_plugin_style.inc for now.
      if ($this->uses_row_plugin() && empty($this->row_plugin)) {
        vpr('views_plugin_style_default: Missing row plugin');
        return;
      }

      // Group the rows according to the grouping field, if specified.
      $sets = $this->render_grouping($this->view->result, $this->options['grouping']);

      // Render each group separately and concatenate.  Plugins may override this
      // method if they wish some other way of handling grouping.
      $output = '';
      foreach ($sets as $title => $records) {
        if ($this->uses_row_plugin()) {
          $rows = array();
          foreach ($records as $label => $row) {
            $rows[] = $this->row_plugin->render($row);
          }
        }
        else {
          $rows = $records;
        }

        $output .= theme($this->theme_functions(), $this->view, $this->options, $rows, $title);
      }
      return $output;
    }
  }
  
  function map_features($rows) {
    $features = array();
    foreach ($this->options['fields']['wkt'] as $field_name => $selected){
      if(!empty($selected)){
        $data_set[$field_name]['wkt'] = array();
        $data = mapstraction_cck_feature_data(content_fields(substr($field_name,0,-4)));
        $data_set[$field_name]['data'] = $data['feature'];
      }
    }
    foreach ($rows as $id => $row) {
      if(!isset($features[$row->nid])){
        $features[$row->nid]['fields'] = $data_set;
      }
      foreach ($this->view->field as $key => $field) {
        if (!isset($processed[$row->nid][$field->field])&& $field->options['exclude'] == 0){
          $features[$row->nid]['bubble'] .= $field->theme($row).'<br>';
          $processed[$row->nid][$field->field] = 1;
        }

        foreach ($this->options['fields']['wkt'] as $field_name){
          if ($key == $this->options['fields']['wkt'][$field_name] && !in_array($field->theme($row),$features[$row->nid]['fields'][$field_name]['wkt'])){
            $field_value = $field->theme($row);
            if(!empty($field_value))$features[$row->nid]['fields'][$field_name]['wkt'][] = $field_value;
          }
        }
      }
    }
    return $features;
  }
}