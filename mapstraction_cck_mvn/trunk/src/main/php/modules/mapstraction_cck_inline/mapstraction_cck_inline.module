<?php
// $Id$

/**
 * @file
 * This file holds the main Drupal hook functions
 * and private functions for the Mapstraction CCK module.
 *
 * @ingroup Geo
 */

/**
 * Implementation of hook_help().
 */
function mapstraction_cck_inline_help($path, $arg) {
  $output = '';

  switch ($path) {
    case 'admin/help#mapstraction_cck_inline':
      $output = '<p>'. t('The Mapstraction CCK Inline module provides input filters to allow for inline Mapstraction maps.') .'</p>';
      return $output;

  }
}

/**
 * Implementation of hook_nodeapi().
 */
function mapstraction_cck_inline_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL){
  if ($op == 'alter'){
    $matches = array();
    if (isset($node->body))$text = $node->body;
    else $text = $node->teaser;
    preg_match_all('/\[(mapstraction[^\]]*)\]/', $text, $matches);
    

    // Check for found
    if (is_array($matches[1]) && count($matches[1]) > 0) {
      foreach ($matches[1] as $i => $match) {
        $items = $map = array();
        $map["id"] = 'mapstraction-cck-inline-node-'.$node->nid.'-field-'.$i;
        $exploded = explode(' ', $match);
        // Check for openlayers prefix
        if ($exploded[0] == 'mapstraction') {
          foreach ($exploded as $value){
            if($value != 'mapstraction'){
              $elements = explode('=',$value);
              switch(trim($elements[0])){
                case 'field':
                  foreach(explode(',',trim($elements[1])) as $this_field){
                    $this_field = 'field_'.$this_field;
                    if(isset($node->$this_field)){
                      if(!isset($field_prop[$this_field])){
                        $field = content_fields($this_field);
                        $this_items = mapstraction_cck_inline_get_field_features($field,$node);
                        $items = array_merge($items,$this_items);
                        $field_prop[$this_field] = $this_items;
                      }else{
                        $items = array_merge($items,$field_prop[$this_field]);
                      }
                    }
                  }
                  break;
                case 'provider':
                  $providers = mapstraction_cck_available_providers();
                  if(isset($providers[trim($elements[1])])) {$map['provider'] = $elements[1];}
                  break;
                case 'georss':
                  $overlay = trim(substr($value,strpos($value, '=')+1));
                  drupal_add_js(array(
                    'mapstraction_formatter' =>array(
                      'maps' => array(
                        $map["id"] => array(
                          'georss' => $overlay,
                        )
                      )
                    )
                  ),'setting');
                  break;
              }
            }
          }
          $map["id"] = 'mapstraction-cck-inline-node-'.$node->nid.'-field-'.$i;
          if(!isset($map["provider"])) {$map["provider"] = $field['widget']['mapstraction_cck']['provider'];}
          $mapstraction_single = array(
            'mapstraction_formatter' =>array(
              'maps' => array(
                $map["id"] => array(
                  'features' => $items,
                    'bbox' => mapstraction_cck_default_bounds(),
                )
              )
            )
          );
          drupal_add_js($mapstraction_single,'setting');
          // Check map
          if (!empty($map)) {
            // Render map
            
            $rendered_map = mapstraction_cck_render_map($map,true);
            $providers[$rendered_map['provider']] = $rendered_map['provider'];
            // Replace text
            $text = replace_first($matches[0][$i], $rendered_map['themed'], $text);
          }
        }
      }
    }
    drupal_add_js(drupal_get_path('module', 'mapstraction_cck') .'/js/mapstraction_cck.js');
    mapstraction_cck_load_maps($rendered_map['provider']);
    if(isset($node->body))$node->body = $text;
    else $node->teaser = $text;
  }
} 

function mapstraction_cck_inline_get_field_features($field, $node){
  $data = mapstraction_cck_feature_data($field);
  $geo = geo_load(array('name' => $field['field_name']));

  // Include the field delta in the query if it's a multi-value field.
  $delta = $field['multiple'] ? ', delta' : '';

  // Result from CCK's select was garbage.  Re-query with AsBinary().
  $res = db_query("SELECT ". $geo->AsBinary($geo) .' AS geo '. $delta ."
    FROM {". $geo->tableName() ."} WHERE vid = %d", $node->vid);

  while ($row = db_fetch_array($res)) {
    $wkt = geo_wkb_get_data(db_decode_blob($row['geo']),'wkt');
    $items[] = array(
      'wkt' => $wkt['value'],
      'data' => $data['feature'],
    );
  }
  return $items;
}

function replace_first($search, $replace, $data) {
    $res = strpos($data, $search);
    if($res === false) {
        return $data;
    } else {
        // There is data to be replaced
        $left_seg = substr($data, 0, strpos($data, $search));
        $right_seg = substr($data, (strpos($data, $search) + strlen($search)));
        return $left_seg . $replace . $right_seg;
    }
}  