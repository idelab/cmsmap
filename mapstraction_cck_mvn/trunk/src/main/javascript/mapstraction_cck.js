// $Id$ 
/**
 * Global Object for Namespace
 */
var Mapstraction = Mapstraction || {};
Mapstraction.CCK = Mapstraction.CCK || {};
Mapstraction.CCK.geocoder = Mapstraction.CCK.geocoder || {};

/**
 * Implementation of Drupal Behavior
 */
Drupal.behaviors.mapstraction_cck = function(context) {
  // Go through OpenLayers data
  if(Mapstraction.isSet(Drupal.settings.mapstraction_cck)){
	  for (var mapid in Drupal.settings.mapstraction_cck.maps) {
	    // Add onblur events to fields
	    $('textarea[rel=' + mapid + ']').blur(function() {
	      Mapstraction.CCK.alterFeatureFromField($(this));
	    });
	  }
  }
};

/**
 * Method that changes the map provider
 * @param mapid
 * 	ID of the map to change
 * @param element
 * 	The div element where the map will be showed
 * @param provider
 * 	The new provider for the map 
 */
Mapstraction.CCK.swapProvider = function(mapid,element,provider){
	Mapstraction.maps[mapid].map.swap(element,provider);
}

/**
 * Initializes the widget map. Add Events to the map and add initial Features
 * @param event
 * 	The event name
 * @param map
 * 	The map to initialize
 */
function populateMap(event,map){
  var featuresToAdd = [];
  var fieldContainer = Mapstraction.CCK.maps[map.mapid].field_container;
  
  var neLat = Mapstraction.CCK.maps[map.mapid].bbox.ne.lat;
  var neLon = Mapstraction.CCK.maps[map.mapid].bbox.ne.lon;
  var swLat = Mapstraction.CCK.maps[map.mapid].bbox.sw.lat;
  var swLon = Mapstraction.CCK.maps[map.mapid].bbox.sw.lon;
  var bbox = new mxn.BoundingBox(swLat,swLon,neLat,neLon);
  
  
  // Cycle through the fieldContainer item and read WKT from all the textareas
  $('#' + fieldContainer + ' textarea').each(function(){
    if ($(this).val() != ''){
      var newFeature = Mapstraction.CCK.loadFeatureFromTextarea(map.mapid, this);
      if (newFeature != false) featuresToAdd.push(newFeature);
    }
  });
  //add geo_type to map
  map.geo_type = Mapstraction.CCK.maps[map.mapid].geo_type;
  var data = Mapstraction.mapDefs[map.mapid].data;

  // Add features to vector
  if (featuresToAdd.length != 0) {
  	for (var i in featuresToAdd)
  	  Mapstraction.addFeature(featuresToAdd[i],map,data);
  }
  Mapstraction.centerMap(Mapstraction.maps[map.mapid].map,bbox);
  var bounds = Mapstraction.maps[map.mapid].map.getBounds();
        var sw = bounds.getSouthWest();
        var ne = bounds.getNorthEast();
       // alert(sw.lat + "| "+swLat+" " + sw.lon + "| "+swLon+" " + ne.lat + "| "+neLat+" " + ne.lon+"| "+neLon+" ");
  
  // Adding new Map events
  map.markerAdded.addHandler(Mapstraction.CCK.featureAdded)
  map.polylineAdded.addHandler(Mapstraction.CCK.featureAdded)
  map.markerRemoved.addHandler(Mapstraction.CCK.featureRemoved)
  map.polylineRemoved.addHandler(Mapstraction.CCK.featureRemoved)
  map.markerChanged.addHandler(Mapstraction.CCK.featureChanged)
  map.polylineChanged.addHandler(Mapstraction.CCK.featureChanged)
  map.markerSelected.addHandler(Mapstraction.CCK.featureSelection)
  map.polylineSelected.addHandler(Mapstraction.CCK.featureSelection)
  map.markerUnselected.addHandler(Mapstraction.CCK.featureSelection)
  map.polylineUnselected.addHandler(Mapstraction.CCK.featureSelection)
  
  //Adding blank textArea to field
  var fieldName = Mapstraction.CCK.maps[map.mapid].field_name_js;
  if($('#' + fieldName + '-items textarea:last').val() != ''){
    var wktFieldAddID = 'edit-' + fieldName + '-' + fieldName + '-add-more';
  	$('#' + wktFieldAddID).trigger('mousedown');  
  }
}



/**
 * apstraction CCK Load Feature From Textarea
 * 
 * This function loads the WKT from a textarea, and returns a feature
 *
 * @param mapid
 *   String ID of map
 * @param textarea
 *   DOM element
 * @return
 *   New feature object or false if error
 */
Mapstraction.CCK.loadFeatureFromTextarea = function(mapid, textarea) {
  var wktFormat = new OpenLayers.Format.WKT();
	// read the wkt values into an OpenLayers geometry object
  var newFeature = wktFormat.read($(textarea).val());

  if (typeof(newFeature) == "undefined"||
    newFeature.geometry.CLASS_NAME.toLowerCase().indexOf(Mapstraction.CCK.maps[mapid].geo_type)<0 ) {
    // TODO: Notification a little less harsh
    //alert(Drupal.t('WKT is not valid'));
    $(textarea).addClass('mapstraction-cck-feature-error')
    return false;
  }
  else {
    // Link the feature to the field.
    newFeature.drupalField = $(textarea).attr('id');
		return newFeature;
  }
}

/**
 * Mapstraction CCK Alter Feature from Field
 * 
 * This will generally be called by an onblur event so that 
 * when users change the raw WKT fields, the map gets updated 
 * in real time
 *
 * @param textarea
 *   DOM element
 */
Mapstraction.CCK.alterFeatureFromField = function(textarea) {
  var mapid = $(textarea).attr('rel');
  var wkt = $(textarea).val();
  var feature = null
  var geo_type =null;
  $(textarea).removeClass('mapstraction-cck-feature-error');
  
  if(Mapstraction.CCK.maps[mapid].geo_type == 'point')
    geo_type = 'markers';
  else
    geo_type = 'polylines'
  // Look for the feature
  for (var l in Mapstraction.maps[mapid].map[geo_type]) {
	  if (Mapstraction.maps[mapid].map[geo_type][l].attributes.drupalField == $(textarea).attr('id')) {
	    var feature = Mapstraction.maps[mapid].map[geo_type][l];
    }
  }
  // Create the new feature
  if ($(textarea).val() != ''){
    var newFeature = Mapstraction.CCK.loadFeatureFromTextarea(mapid, textarea);
    if(newFeature!= false){
      if(feature !=null){
      	Mapstraction.updateFeature(feature,newFeature);
      }else
        Mapstraction.addFeature(newFeature, Mapstraction.maps[mapid].map);
    }
  }else{
  	if(feature != null)Mapstraction.removeFeature(feature);
  }
  
}

/**
 * Mapstraction CCK Feature Selected
 * 
 * When a feature is selected, make the WKT field light 
 * up so we know which field we are editing
 *
 * @param event
 *   Event object
 */
Mapstraction.CCK.featureSelection = function(event,map,feature) {
  if(Mapstraction.isSet(feature.marker)){
    var drupalField = feature.marker.attributes.drupalField;
  }else{
  	var drupalField = feature.polyline.attributes.drupalField
  }
  if(event == "markerSelected"||event == "polylineSelected")
  	$("#" + drupalField).addClass('mapstraction-cck-feature-selected');
  else
    $("#" + drupalField).removeClass('mapstraction-cck-feature-selected');
}

/**
 * Mapstraction CCK Feature Added
 * 
 * When a feature is added, shows his WKT value in a textarea in the widget form
 * 
 * @param event
 *   Event name
 * @param map
 * 	Map where the event is fired
 * @param feature
 * 	The feature that has been added
 */
Mapstraction.CCK.featureAdded = function(event,map,feature){
  var fieldName = Mapstraction.CCK.maps[map.mapid].field_name_js;
  var container = Mapstraction.CCK.maps[map.mapid].field_container;
  var multiple = Mapstraction.CCK.maps[map.mapid].multiple;	
	// Get the index number of the newly added field
  // Check if we are creating a new node with 2 fresh fields open
  if (multiple == 0){
  	var newNode = true;
  	var newFeatureID = 0
  }else{
	  if ($('#' + fieldName + '-items textarea').size() == 2 && $('#' + fieldName + '-items textarea:first').val() == '') {
	  	// We are creating a new node with two fresh fields open
	  	var newNode = true;
	  	var newFeatureID = 0;
	  }
	  else {
	  	// We are either not creating a new node, or there is already data filled in.
	  	var newNode = false;
	  	var fields = $('#' + container + ' textarea')
	  	for (var i in fields){
	  	  if(fields[i].value ==''){
	  		var newFeatureID = i;
	  		if (i < fields.size() - 1) newNode = true;
	  		break;
	  	  }
	  	}
	  	//var newFeatureID = $('#' + fieldName + '-items textarea').size() - 1;
	  }
  }
  // This is the id of the textfield we will be assigning this feature to.
  var wktFieldNewID = 'edit-' + fieldName + '-' + newFeatureID + '-geo-wkt';
  // This is the "Add another item" button
  var wktFieldAddID = 'edit-' + fieldName + '-' + fieldName + '-add-more';
  
  //Defining if feature is a Marker or a Polyline
  if(Mapstraction.isSet(feature.marker)){
    feature = feature.marker
  }else{
  	feature = feature.polyline
  }
  // Assign field to feature
  feature.attributes.drupalField = wktFieldNewID;
  
  var openFeature = feature.toOpenLayers().geometry;
  // Update CCK field with WKT values
  

  openFeature.transform(new OpenLayers.Projection("EPSG:900913"),new OpenLayers.Projection("EPSG:4326"));
  
  $('#' + wktFieldNewID).val(openFeature.toString());
  
  // Link the field to the map
  $('#' + wktFieldNewID).attr('rel',map.mapid);
    
  // Add another field if we need to..
  if (!newNode && multiple == 1) $('#' + wktFieldAddID).trigger('mousedown');  
  
}


/**
 * Mapstraction CCK Feaure Modified Handler
 * 
 * When the any feature on the layer is modified, fill in 
 * the WKT values into the text fields.
 *
 * @param event
 *   Event name
 * @param map
 * 	Map where the event is fired
 * @param feature
 * 	The feature that has been modified
 */
Mapstraction.CCK.featureChanged = function(event,map,feature) {
  if(Mapstraction.isSet(feature.marker)){
    var openFeature = feature.marker.toOpenLayers().geometry;
    var drupalField = feature.marker.attributes.drupalField;
  }else{
  	var openFeature = feature.polyline.toOpenLayers().geometry;
  	var drupalField = feature.polyline.attributes.drupalField;
  }
  openFeature.transform(new OpenLayers.Projection("EPSG:900913"),new OpenLayers.Projection("EPSG:4326"));
  
  // update CCK fields
  var wkt = openFeature.toString();
  $('#' + drupalField).val(wkt);
}

/**
 * Mapstraction CCK Feature Removed Handler
 * 
 * When the any feature on the layer is deleted, strip out the 
 * WKT values from the CCK value fields.
 *
 * @param event
 *   Event object
 * @param map
 * 	Map where the event is fired
 * @param feature
 * 	The feature that has been removed
 */
Mapstraction.CCK.featureRemoved = function(event,map,feature) {
  if(Mapstraction.isSet(feature.marker))
    feature = feature.marker;
  else
    feature = feature.polyline 
  // Empty the CCK field values.
  $('#' + feature.attributes.drupalField).val('').removeClass('mapstraction-cck-feature-selected');
}

/**
 * Mapstraction CCK Add Feature Control
 * 
 * When there are the max number of features in the map, it shows an error message and doesn't activate the control 
 * 
 * @param mapid
 *   The map ID
 */
Mapstraction.CCK.countFeatures = function(mapid){
  if(Mapstraction.CCK.maps[mapid].multiple == 1){
    Mapstraction.maps[mapid].map.addFeature(Mapstraction.CCK.maps[mapid].geo_type,Mapstraction.mapDefs[mapid].data);
  }else{
  	if(Mapstraction.CCK.maps[mapid].multiple == 0)
  	  var max = 1;
  	else
	  var max = Mapstraction.CCK.maps[mapid].multiple
	
	if(Mapstraction.CCK.maps[mapid].geo_type == 'point')
	  var type = 'markers';
	else
	  var type = 'polylines';
	
	if(Mapstraction.maps[mapid].map[type].length < max)
	  Mapstraction.maps[mapid].map.addFeature(Mapstraction.CCK.maps[mapid].geo_type,Mapstraction.mapDefs[mapid].data);
	else{
	  $('#'+mapid+'-count-msg').show("slow");
	  setTimeout("$('#"+mapid+"-count-msg').hide('fast');",3000);	
	}
	  	  
  }
}

Mapstraction.CCK.geocoder.callback = function(geocoded_location){
  var zoom = Mapstraction.maps[Mapstraction.CCK.geocoder.mapid].map.getZoom();
  if (zoom < 12) zoom  = 12;
  else zoom = zoom + 2; 	
  Mapstraction.maps[Mapstraction.CCK.geocoder.mapid].map.setCenterAndZoom(geocoded_location.point,zoom);
}
/**
 * Onload page event which starts the map proccessing
 */
jQuery(document).ready(function() {
  
  if(Mapstraction.isSet(Drupal.settings.mapstraction_cck)){
  		
	  Mapstraction.CCK.maps = Drupal.settings.mapstraction_cck.maps;
		
	  // Go through CCK Fields and diplay map
	  var fieldContainer = '';
	  for (var mapid in Mapstraction.CCK.maps) {
	  	Drupal.settings.mapstraction.maps[mapid].handlers = {};
	    fieldContainer = Mapstraction.CCK.maps[mapid].field_container;
	    // Add Themed Map container
	    $('#' + fieldContainer).before(Mapstraction.CCK.maps[mapid].field_map_themed);
	    //if(mapid.indexOf("config") >=0) continue;
	    $('#' + fieldContainer).hide();
	        
	    // Define click actions for WKT Switcher
	    $('#' + mapid + '-wkt-switcher').click(function() {
	      var mapid = $(this).attr('rel');
	      var fieldContainer = Mapstraction.CCK.maps[mapid].field_container;
	      $('#' + fieldContainer).toggle('normal');
	      return false;
	    });
	    // Define click actions for Map Controls
	    $('#' + mapid + '-add-feature').click(function() {
	      var mapid = $(this).attr('rel');
	      Mapstraction.CCK.countFeatures(mapid);
	      //Mapstraction.maps[mapid].map.addFeature(Mapstraction.CCK.maps[mapid].geo_type,Mapstraction.mapDefs[mapid].data);
	      return false;
	    });
	    // Define click actions for Map Controls
	    $('#' + mapid + '-activate-edition').click(function() {
	      var mapid = $(this).attr('rel');
	      Mapstraction.maps[mapid].map.activateEdition()
	      return false;
	    });
	    // Define click actions for Map Controls
	    $('#' + mapid + '-deactivate-edition').click(function() {
	      var mapid = $(this).attr('rel');
	      Mapstraction.maps[mapid].map.deactivateEdition();
	      return false;
	    });
	    if(Mapstraction.CCK.maps[mapid].geocode != 'none'){
	      if(!Mapstraction.isSet(Mapstraction.CCK.geocoder.component)){
	      	Mapstraction.CCK.geocoder.component = new mxn.MapstractionGeocoder(Mapstraction.CCK.geocoder.callback,Mapstraction.CCK.maps[mapid].geocode);
	      }	
	      // Define click actions for Geocode button
	      $('#' + mapid + '-geocode-button').click(function() {
	        var mapid = $(this).attr('rel');
	        Mapstraction.CCK.geocoder.component.swap(Mapstraction.CCK.maps[mapid].geocode);
	        var address = {}; 
	        address.address = $('#' + mapid + '-geocode-field').val();
	        Mapstraction.CCK.geocoder.mapid = mapid;
	        Mapstraction.CCK.geocoder.component.geocode(address);
	        return false;
	      });
	    }
	  	Drupal.settings.mapstraction.maps[mapid].handlers.load = populateMap
	  }  
	  
	  Drupal.attachBehaviors();
  }else{
  	for (var mapid in Drupal.settings.mapstraction_formatter.maps){
  	  if(!Mapstraction.isSet(Mapstraction.maps[mapid]))continue;
  	  var map = Mapstraction.maps[mapid].map;
  	  var wktFormat = new OpenLayers.Format.WKT();
  	  for (var i in Drupal.settings.mapstraction_formatter.maps[mapid].features){
  	  	// read the wkt values into an OpenLayers geometry object
 		var newFeature = wktFormat.read(Drupal.settings.mapstraction_formatter.maps[mapid].features[i].wkt);
  	  	var data = Drupal.settings.mapstraction_formatter.maps[mapid].features[i].data;
  	  	if(Mapstraction.isSet(newFeature)) 	
  	  	Mapstraction.addFeature(newFeature,Mapstraction.maps[mapid].map,data)
  	  }
  	  var bounds = Drupal.settings.mapstraction_formatter.maps[mapid].bbox;
  	  var bbox = new mxn.BoundingBox( bounds.sw.lat, bounds.sw.lon, bounds.ne.lat, bounds.ne.lon);
  	  Mapstraction.centerMap(Mapstraction.maps[mapid].map,bbox);
	  if(Mapstraction.isSet(Drupal.settings.mapstraction_formatter.maps[mapid].georss)){
	  	Mapstraction.addOverlay(Mapstraction.maps[mapid].map,Drupal.settings.mapstraction_formatter.maps[mapid].georss);
	  }
  	  //$('.field-field-polygon')[0].children[0].textContent = 'dfdsfas'
  	}
	
  }
  
});