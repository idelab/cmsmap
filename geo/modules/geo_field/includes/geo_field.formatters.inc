<?php // $Id: geo_field.formatters.inc,v 1.0 2009/05/20 13:16:28 chiru Exp $
/**
 * @file
 * Field formatters for geo_field fields.
 */
function theme_geo_field_formatter_wkt_geocode($element) {
  if (isset($element['#item']['wkt'])&&isset($element['#item']['geocode'])&&$element['#item']['geocode']!=""){
    //drupal_add_js('items="'.$element['#item']['wkt']."\"","inline");
    return $element['#item']['wkt']."--->".$element['#item']['geocode'];
  } 
  if (isset($element['#item']['wkt'])) return $element['#item']['wkt'];
}

